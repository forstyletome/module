<?
	$MESS["XUNIT_XMLDROPSHIPPING_FILTER_FIND_ID"] = "ID поставщика";
	$MESS["XUNIT_XMLDROPSHIPPING_FILTER_FIND_NAME"] = "Наименование поставщика";
	$MESS["XUNIT_XMLDROPSHIPPING_EDIT"] = "Редактировать";
	$MESS["XUNIT_XMLDROPSHIPPING_DELETE"] = "Удалить";
	$MESS["XUNIT_XMLDROPSHIPPING_DELETE_SUCCESS"] = "Поставщик успешно удалён";
	$MESS["XUNIT_XMLDROPSHIPPING_ADD_SELLER"] = "Добавить поставщика";
	$MESS["XUNIT_XMLDROPSHIPPING_PAGES"] = "Страница";