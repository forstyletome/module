<?
	$MESS["XUNIT_XMLDROPSHIPPING_EDIT_TAB_1"] = "Информация";
	$MESS["XUNIT_XMLDROPSHIPPING_EDIT_TAB_1_1"] = "Добавление или изменение информации о поставщике";
	$MESS["XUNIT_XMLDROPSHIPPING_EDIT_TAB_2"] = "XSD схема";
	$MESS["XUNIT_XMLDROPSHIPPING_EDIT_TAB_2_1"] = "Добавление или изменение XSD схемы для анализа прайс-листов";
	$MESS["XUNIT_XMLDROPSHIPPING_EDIT_TAB_3"] = "Прайслисты на нашем сервере";
	$MESS["XUNIT_XMLDROPSHIPPING_EDIT_TAB_3_1"] = "Добавление/Удаление/Анализ прайс-листов на нашем сервере";
	$MESS["XUNIT_XMLDROPSHIPPING_EDIT_TAB_4"] = "Прайслисты на удалённом сервере";
	$MESS["XUNIT_XMLDROPSHIPPING_FORM_NAME_SELLER"] = "Наименование поставщика:";
	$MESS["XUNIT_XMLDROPSHIPPING_FORM_FIO_SELLER"] = "ФИО менеджера:";
	$MESS["XUNIT_XMLDROPSHIPPING_FORM_DESCRIPTION_SELLER"] = "Комментарий:";
	$MESS["XUNIT_XMLDROPSHIPPING_ADD_SELLER"] = "Добавить поставщика:";
	$MESS["XUNIT_XMLDROPSHIPPING_FORM_TELEPHONE_SELLER"] = "Телефон:";
	$MESS["XUNIT_XMLDROPSHIPPING_DELETE"] = "Удалить";
	$MESS["XUNIT_XMLDROPSHIPPING_UPDATE"] = "Обновление данных поставщика выполнено успешно";
	$MESS["XUNIT_XMLDROPSHIPPING_FORM_LOCATION_URL"] = "Расположение файла";
	$MESS["XUNIT_XMLDROPSHIPPING_LIST_SELLER"] = "Вернуться в список поставщиков";
	$MESS["XUNIT_XMLDROPSHIPPING_FORM_SITE_SELLER"] = "Сайт поставщика:";
