<?
$MESS["XUNIT_XMLDROPSHIPPING_MODULE_NAME"] = "Дропшиппинг";
$MESS["XUNIT_XMLDROPSHIPPING_MODULE_DESC"] = "Парсинг и настройка парсинга xml-файлов от партнёров";
$MESS["XUNIT_XMLDROPSHIPPING_PARTNER_NAME"] = "Богачёв Виталий";
$MESS["XUNIT_XMLDROPSHIPPING_PARTNER_URI"] = "https://x-unit.com.ua/";
$MESS["XUNIT_XMLDROPSHIPPING_UNINSTALL_TITLE"] = "Удаление";

$MESS["XUNIT_XMLDROPSHIPPING_DENIED"] = "Доступ закрыт";
$MESS["XUNIT_XMLDROPSHIPPING_READ_COMPONENT"] = "Доступ к компонентам";
$MESS["XUNIT_XMLDROPSHIPPING_WRITE_SETTINGS"] = "Изменение настроек модуля";
$MESS["XUNIT_XMLDROPSHIPPING_FULL"] = "Полный доступ";

$MESS["XUNIT_XMLDROPSHIPPING_INSTALL_TITLE"] = "Установка модуля";
$MESS["XUNIT_XMLDROPSHIPPING_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";