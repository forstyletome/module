<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class ReportParsingExtendTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_xml_report_parsing_extend';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// LAST_ID IMPORT
            new Entity\IntegerField('LAST_ID_IMPORT', array(
                'required' => true,
            )),
			
			// ID прайс-листа
            new Entity\IntegerField('ID_PRICE_LIST', array(
                'required' => true,
            )),
			
			// Ошибки прайс-листа
			new Entity\TextField('ERRORS_PRICE_LIST'),
			
			// Отличие в свойствах и категориях
			new Entity\TextField('DIFFERENCE'),
			
        );
		
    }
	
}