<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class XmlTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_xml';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// ID продавца
            new Entity\IntegerField('ID_SELLER', array(
                'required' => true,
            )),
			
			// ID XSD схемы
            new Entity\IntegerField('ID_XSD', array(
                'required' => true,
            )),
			
			// Наименование прайс-листа
            new Entity\StringField('FILE_XML_NAME_SELLER', array(
                'required' => true,
            )),
			
			// URL прайс-листа
			new Entity\StringField('FILE_XML_LINK_SELLER', array(
                'required' => true,
            )),
			
			// URL прайс-листа поставщика
			new Entity\StringField('FILE_XML_LINK_SELLER_LINK', array(
                'required' => true,
            )),
			
			/*
			// Статус анализа
            new Entity\IntegerField('STATUS_ANALIZE', array(
                'required' 	=> true, // 1 - Стартовая позиция, 2 - Старт анализа, 3 - Идёт анализ, 4 - Анализ завершён
            )),
			
			// Наличие ошибок в прайс-листе
			new Entity\StringField('FOUND_ERRORS'),
			
			// Текст ошибок в прайс-листе
			new Entity\TextField('ERRORS_PRICE_LIST'),
			
			// Статус анализа свойств
            new Entity\IntegerField('STATUS_ANALIZE_PROPERTIES', array(
                'required' 	=> true, // 1 - Стартовая позиция, 2 - Старт анализа, 3 - Идёт анализ, 4 - Анализ завершён
            )),
			*/
			
			// Структура файла
			new Entity\TextField('STRUCTURE_PRICELIST'),
			
			// Свойства товаров
			new Entity\TextField('PROPERTIES_OFFER_PRICELIST')
			
        );
		
    }
	
	public function getSectionList(){
		
		$arFilter = array(		
			'ACTIVE' 		=> 'Y',
			'IBLOCK_ID' 	=> 6,
			'GLOBAL_ACTIVE'	=>'Y',
		);
		
		$arSelect = array('IBLOCK_ID', 'ID', 'NAME', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID', 'UF_IBLOCK');
		$arOrder = array('DEPTH_LEVEL' => 'ASC', 'SORT' => 'ASC');
		$rsSections = \CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
		$sectionLinc = array();
		$arResult['ROOT'] = array();
		$sectionLinc[0] = &$arResult['ROOT'];
		
			while($arSection = $rsSections->GetNext()){
				$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']] = $arSection;
				$sectionLinc[$arSection['ID']] = &$sectionLinc[intval($arSection['IBLOCK_SECTION_ID'])]['CHILD'][$arSection['ID']];
			}
			
			unset($sectionLinc);
			
		return $arResult["ROOT"]["CHILD"];

	}
	
}