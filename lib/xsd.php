<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class XsdTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_xsd';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			 // ID продавца
            new Entity\IntegerField('ID_SELLER', array(
                'required' => true,
            )),
			
			// Наименование XSD схемы
            new Entity\StringField('FILE_XSD_NAME_SELLER', array(
                'required' => true,
            )),
			
			// URL XSD файла
			new Entity\StringField('FILE_XSD_LINK_SELLER', array(
                'required' => true,
            ))
			
        );
		
    }
	
}