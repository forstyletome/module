<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class LogParsingTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_log_parsing';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// ID импорта
            new Entity\IntegerField('LAST_ID_IMPORT', array(
                'required' => true,
            )),
			
            // ID прайс-листа
            new Entity\IntegerField('ID_PRICE_LIST', array(
                'required' => true,
            )),
			
			// ID поставщика
            new Entity\IntegerField('ID_SELLER', array(
                'required' => true,
            )),

			// Статус: // ERROR/ADD/UPDATE/REMOVE/PASS
            new Entity\StringField('STATUS_PARSING', array(
                'required' => true,
            )),
			
			// Статус расширенный:
            new Entity\StringField('STATUS_PARSING_EXTEND', array(
                'required' => true,
            )),
			
			// ID товара
            new Entity\StringField('ID_PRODUCT', array(
                'required' => true,
            )),
			
			// Комментарий
            new Entity\StringField('COMMENT')
			
        );
		
    }
	
}