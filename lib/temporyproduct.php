<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class TemporyProductTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_temp_product_list';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// LAST ID IMPORT 
            new Entity\IntegerField('LAST_ID_IMPORT', array(
                'required' => true,
            )),
			
            // ID прайс-листа
            new Entity\StringField('ID_PRICE_LIST', array(
                'required' => true,
            )),
			
			// ID поставщика
            new Entity\StringField('ID_SELLER', array(
                'required' => true,
            )),

			// Структура товара с его данными
            new Entity\TextField('PRODUCT')
			
        );
		
    }
	
}