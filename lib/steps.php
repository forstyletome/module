<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class StepsTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_steps';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// ID продавца
            new Entity\IntegerField('ID_SELLER'),
			
			// ID XSD схемы
            new Entity\IntegerField('ID_XSD'),
			
			// ID Прайс-листа
            new Entity\IntegerField('ID_XML'),
			
			// Настройки - Да/Нет
            new Entity\StringField('ISSET_SETTINGS', array(
                'required' => true,
            )),
			
			// Анализ прайс-листа - Да/Нет
            new Entity\StringField('ANALIZE_PRICE_LIST', array(
                'required' => true,
            ))
			
        );
		
    }
	
}