<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class SettingsPriceListImportTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_settings';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// ID прайс-листа
            new Entity\IntegerField('ID_XML', array(
                'required' => true,
            )),
			
			// Настройки прайс-листа
			new Entity\TextField('SETTINGS_PRICELIST')
			
        );
		
    }
	
}