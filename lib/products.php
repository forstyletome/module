<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class ProductsTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_products';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// ID продавца
            new Entity\IntegerField('ID_SELLER', array(
                'required' => true,
            )),
			
			// ID прайс-листа
            new Entity\StringField('ID_XML', array(
                'required' => true,
            )),
			
			// ID товара в прайс-листе
			new Entity\StringField('ID_OFFER_IN_PRICE_LIST', array(
                'required' => true,
            )),
			
			// ID товара на сайте
			new Entity\IntegerField('ID_PRODUCT_IN_SITE'),
			
			// ID торгового предложения на сайте
			new Entity\IntegerField('ID_OFFER_IN_SITE'),
			
			// ID инфоблока торговых предложений
			new Entity\IntegerField('ID_IBLOCK_OFFER'),
			
			// ID инфоблока товара
			new Entity\IntegerField('ID_IBLOCK_PRODUCT'),
			
			// Первое обновление всего кроме фотографий = 1 или 0
			new Entity\IntegerField('FIRST_UPDATE'),
			
			// Первое обновление фотографий = 1 или 0
			new Entity\IntegerField('FIRST_UPDATE_PHOTOS')
			
        );
		
    }
	
}