<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class PricepropertiesTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_price_properties';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// ID прайс-листа
            new Entity\IntegerField('ID_XML', array(
                'required' => true,
            )),
			
			// ID инфоблока
            new Entity\IntegerField('ID_IBLOCK', array(
                'required' => true,
            )),
			
			// Тип инфоблока(IBLOCK, OFFER, OTHER)
            new Entity\StringField('TYPE_IBLOCK', array(
                'required' => true,
            )),
			
			// Свойство
			new Entity\StringField('NAME_PROPERTIES', array(
                'required' => true,
            )),
			
			// Значение свойства
			new Entity\TextField('VALUE_PROPERTIES', array(
                'required' => true,
            ))
			
        );
		
    }
	
}