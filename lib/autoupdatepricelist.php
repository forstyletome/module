<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class AutoUpdatePriceListTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_auto_update_settings';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// ID продавца
            new Entity\IntegerField('ID_SELLER', array(
                'required' => true,
            )),
			
			// ID Прайс-листа
            new Entity\IntegerField('ID_XML', array(
                'required' => true,
            )),
			
			// Активно или нет
            new Entity\StringField('ACTIVE', array(
                'required' => true,
            )),
			
			// Настройка промежутка времени
            new Entity\StringField('TIME_SETTING', array(
                'required' => true,
            )),
			
			// ID агента
            new Entity\StringField('ID_AGENT')
			
        );
		
    }
	
}