<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class ReportParsingTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_xml_report_parsing';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// ID прайс-листа
            new Entity\IntegerField('ID_PRICE_LIST', array(
                'required' => true,
            )),
			
			// ID поставщика
            new Entity\IntegerField('ID_SELLER', array(
                'required' => true,
            )),
			
			// LAST ID IMPORT 
            new Entity\IntegerField('LAST_ID_IMPORT', array(
                'required' => true,
            )),
			
			// Дата и время шага
			new Entity\StringField('DATE_TIME_IMPORT', array(
                'required' => true,
            )),
			
			// Задача
			new Entity\StringField('TASK', array(
                'required' => true,
            )),
			
			// Статус шага импорта
            new Entity\IntegerField('STEP', array(
                'required' => true,
            )),
			
			// Текст шага импорта
            new Entity\StringField('TEXT_IMPORT', array(
                'required' => true,
            )),
			
			// Успешен ли шаг ? Y || N
            new Entity\StringField('RESULT_STEP', array(
                'required' => true,
            )),
			
			// Отменён импорт ? Y || N
            new Entity\StringField('CANCEL_IMPORT', array(
                'required' => true,
            ))
			
        );
		
    }
	
}