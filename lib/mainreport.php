<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class MainReportTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_main_report';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// Дата и время
            new Entity\DateTimeField('DATE_TIME', array(
                'default_value' => new \Bitrix\Main\Type\DateTime()
            )),
			
			// ID продавца
            new Entity\IntegerField('ID_SELLER'),
			
			// ID XSD схемы
            new Entity\IntegerField('ID_XSD'),
			
			// ID прайс-листа
            new Entity\IntegerField('ID_XML'),
			
			// Шаг
            new Entity\IntegerField('STEP', array(
                'required' => true,
            )),
			
			// Задача
            new Entity\StringField('TASK', array(
                'required' => true,
            )),
			
			// Статус
            new Entity\StringField('STATUS', array(
                'required' => true,
            )),
			
			// Текст ошибок в прайс-листе
			new Entity\TextField('COMMENTS_OR_ERRORS')
			
        );
		
    }
	
}