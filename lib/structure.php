<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class StructureTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_structure';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// ID прайс-листа
            new Entity\IntegerField('ID_XML', array(
                'required' => true,
            )),
			
			// ID категории в прайс-листе
            new Entity\IntegerField('ID_CATEGORY_XML', array(
                'required' => true,
            )),
			
			// ID категории на нашем сайте
			new Entity\IntegerField('ID_CATEGORY_SITE', array(
                'required' => true,
            ))
			
        );
		
    }
	
}