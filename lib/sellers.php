<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class SellersTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
            // Название компании продавца
            new Entity\StringField('NAME_SELLER', array(
                'required' => true,
            )),
			
			// ФИО менеджера у продавца
            new Entity\StringField('FIO_SELLER'),
			
			// ФИО менеджера у продавца
            new Entity\StringField('SITE_SELLER'),
			
			// ФИО менеджера у продавца
            new Entity\StringField('TELEPHONE_SELLER'),
			
			// Комментарии менеджера
            new Entity\TextField('DESCRIPTION_SELLER')
			
        );
		
    }
	
}

/*

class PriceListTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'pricelist_xunit';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// Название прайс-листа
            new Entity\StringField('NAME_PRICE_LIST'),
			
            // ID поставщика
            new Entity\IntegerField('ID_SELLER', array(
                'required' => true,
            )),
			
			// Ссылка на файл
            new Entity\StringField('LINK_TO_PRICE'),
			
			// Был ли проведён анализ файла
            new Entity\BooleanField(
				'ANALIZ',
				[
					'values' => ['N', 'Y']
				]
			),
			
			// Был ли проведён анализ файла
            new Entity\BooleanField(
				'GO_TO_ANALIZ',
				[
					'values' => ['N', 'Y']
				]
			),
			
			// Файл находится на сервере 
            new Entity\BooleanField(
				'LOCATION',
				[
					'values' => ['N', 'Y']
				]
			),
			
			// Есть ли в файле ошибки 
            new Entity\BooleanField(
				'FOUND_ERRORS',
				[
					'values' => ['N', 'Y']
				]
			),
			
			// Список ошибок
            new Entity\TextField('ERRORS_PRICE_LIST'),
			
			// Свойства у прайс-листа
            new Entity\StringField('PROPERTIES_PRICE_LIST'),
			
			// Настройки прайс-листа
            new Entity\StringField('SETTINGS_PRICE_LIST'),

        );
		
    }
	
}

class Sellers{
	
	public function getByIdPriceList($priceListId){
		
		$rsDataPrices = \Xunit\Xmldropshipping\PriceListTable::getById($priceListId);
		
		$resPrices = $rsDataPrices->Fetch();
		
		return $resPrices;
		
	}
	
	public function getInfoSeller($idSeller){
		
		$seller = [];
		
		// Общая информация по поставщику
		$rsData = \Xunit\Xmldropshipping\SellersTable::getList(
			[
				'select'	=> ["*"],
				'filter'	=> ["ID" => $idSeller]
			]
		);
		
		$res = $rsData->Fetch();

		$seller["NAME"] = $res["NAME"];
		$seller["DESCRIPTION"] = $res["DESCRIPTION"];
		$seller["FIO_SELLERS"] = $res["FIO_SELLERS"];
		$seller["PRICELIST"] = self::getInfoPriceListByIdSeller($idSeller);
		
		return $seller;
		
	}
	
	public function getInfoPriceListByIdSeller($idSeller){
		
		$seller = [];
		
		// Прайс-листы поставщика и их настройки
		$rsDataPrices = \Xunit\Xmldropshipping\PriceListTable::getList(
			[
				'select'	=> ["*"],
				'filter'	=> ["ID_SELLER" => $idSeller]
			]
		);
		
		$resPrices = $rsDataPrices->FetchAll();

		if($resPrices){

			foreach($resPrices as $priceList){
				
				$seller[] = [
					"ID" 					=> $priceList["ID"],
					"NAME_PRICE_LIST"		=> $priceList["NAME_PRICE_LIST"],
					"ID_SELLER"				=> $priceList["ID_SELLER"],
					"LOCATION"				=> $priceList["LOCATION"],
					"LINK_TO_PRICE"			=> $priceList["LINK_TO_PRICE"],
					"ANALIZ"				=> $priceList["ANALIZ"],
					"GO_TO_ANALIZ"			=> $priceList["GO_TO_ANALIZ"],
					"PROPERTIES_PRICE_LIST" => $priceList["PROPERTIES_PRICE_LIST"],
					"SETTINGS_PRICE_LIST"	=> $priceList["SETTINGS_PRICE_LIST"],
					"FOUND_ERRORS"			=> $priceList["FOUND_ERRORS"],
					"ERRORS_PRICE_LIST"		=> $priceList["ERRORS_PRICE_LIST"]
				];
				
			}
		
		}
		
		return $seller;
		
	}
	
	public function getInfoPriceListByFilterSeller($select = ['*'], $filter){
		
		$seller = [];
		
		// Прайс-листы поставщика и их настройки
		$rsDataPrices = \Xunit\Xmldropshipping\PriceListTable::getList(
			[
				'select'	=> $select,
				'filter'	=> $filter
			]
		);
		
		$resPrices = $rsDataPrices->FetchAll();

		if($resPrices){

			foreach($resPrices as $priceList){
				
				$seller[$priceList["ID"]] = [
					"ID" 					=> $priceList["ID"],
					"NAME_PRICE_LIST"		=> $priceList["NAME_PRICE_LIST"],
					"ID_SELLER"				=> $priceList["ID_SELLER"],
					"LOCATION"				=> $priceList["LOCATION"],
					"LINK_TO_PRICE"			=> $priceList["LINK_TO_PRICE"],
					"ANALIZ"				=> $priceList["ANALIZ"],
					"GO_TO_ANALIZ"			=> $priceList["GO_TO_ANALIZ"],
					"PROPERTIES_PRICE_LIST" => $priceList["PROPERTIES_PRICE_LIST"],
					"SETTINGS_PRICE_LIST"	=> $priceList["SETTINGS_PRICE_LIST"],
					"FOUND_ERRORS"			=> $priceList["FOUND_ERRORS"],
					"ERRORS_PRICE_LIST"		=> $priceList["ERRORS_PRICE_LIST"]
				];
				
			}
		
		}
		
		return $seller;
		
	}
	
	public function updatePriceListSeller($params){
		
		$priceId = $params["PRICE_ID"];
		
		unset($params["PRICE_ID"]);
		
		$res = \Xunit\Xmldropshipping\PriceListTable::update(
			$priceId,
			$params
		);
		
	}
	
}

*/