<?

namespace Xunit\Xmldropshipping;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class ComparePropertiesTable extends Entity\DataManager{
	
    public static function getTableName(){
		
        return 'sellers_import_xunit_compare_properties';
		
    }
	
	public static function getMap(){
		
        return array(
            
			// ID
            new Entity\IntegerField('ID', array(
                'primary' 		=> true,
                'autocomplete' 	=> true
            )),
			
			// ID продавца
            new Entity\IntegerField('ID_SELLER'),
			
			// ID XSD схемы
            new Entity\IntegerField('ID_XSD'),
			
			// ID Прайс-листа
            new Entity\IntegerField('ID_XML'),
			
			// Свойство в прайс-листе
            new Entity\StringField('PROPERTIES_IN_PRICE_LIST', array(
                'required' => true,
            )),
			
			// Свойство сайта
            new Entity\StringField('PROPERTIES_IN_SITE', array(
                'required' => true,
            ))
			
        );
		
    }
	
}