<?

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config as Conf;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity\Base;
use \Bitrix\Main\Application;

Loader::includeModule("catalog");

Loc::LoadMessages(__FILE__);

Class xunit_xmldropshipping extends CModule{

    function __construct(){

        $arModuleVersion = [];
        include(__DIR__."/version.php");

        $this->MODULE_ID = "xunit.xmldropshipping";
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = Loc::getMessage("XUNIT_XMLDROPSHIPPING_MODULE_NAME");
        $this->MODULE_DESCRIPTION = Loc::getMessage("XUNIT_XMLDROPSHIPPING_MODULE_DESC");
        $this->PARTNER_NAME = Loc::getMessage("XUNIT_XMLDROPSHIPPING_PARTNER_NAME");
        $this->PARTNER_URI = Loc::getMessage("XUNIT_XMLDROPSHIPPING_PARTNER_URI");
        $this->MODULE_SORT = 1;
		
        $this->SHOW_SUPER_ADMIN_GROUP_RIGHTS = "Y";
        $this->MODULE_GROUP_RIGHTS = "Y";

    }
	
	function InstallDB(){
		
        Loader::includeModule($this->MODULE_ID);

        if(!Application::getConnection(\Xunit\Xmldropshipping\SellersTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\SellersTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\SellersTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\XsdTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\XsdTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\XsdTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\XmlTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\XmlTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\XmlTable')->createDbTable();

			global $DB;
			//$DB->Query("ALTER TABLE `sellers_import_xunit_xml` MODIFY ERRORS_PRICE_LIST LONGTEXT");
			$DB->Query("ALTER TABLE `sellers_import_xunit_xml` MODIFY STRUCTURE_PRICELIST LONGTEXT");
			
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\StructureTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\StructureTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\StructureTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\PricepropertiesTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\PricepropertiesTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\PricepropertiesTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\SettingsPriceListImportTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\SettingsPriceListImportTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\SettingsPriceListImportTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\ReportParsingTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\ReportParsingTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\ReportParsingTable')->createDbTable();
        }
		
		/*
		if(!Application::getConnection(\Xunit\Xmldropshipping\ReportParsingExtendTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\ReportParsingExtendTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\ReportParsingExtendTable')->createDbTable();
			
			global $DB;
			$DB->Query("ALTER TABLE `sellers_import_xunit_xml_report_parsing_extend` MODIFY DIFFERENCE LONGTEXT");
			
        }
		*/
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\TemporyProductTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\TemporyProductTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\TemporyProductTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\LogParsingTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\LogParsingTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\LogParsingTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\MainReportTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\MainReportTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\MainReportTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\StepsTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\StepsTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\StepsTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\ProductsTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\ProductsTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\ProductsTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\ComparePropertiesTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\ComparePropertiesTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\ComparePropertiesTable')->createDbTable();
        }
		
		if(!Application::getConnection(\Xunit\Xmldropshipping\AutoUpdatePriceListTable::getConnectionName())->isTableExists(
            Base::getInstance('\Xunit\Xmldropshipping\AutoUpdatePriceListTable')->getDBTableName()
            )
        ){
            Base::getInstance('\Xunit\Xmldropshipping\AutoUpdatePriceListTable')->createDbTable();
        }

	}
	
	function UnInstallDB(){
		
        Loader::includeModule($this->MODULE_ID);

        Application::getConnection(\Xunit\Xmldropshipping\SellersTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\SellersTable')->getDBTableName());
		
		Application::getConnection(\Xunit\Xmldropshipping\XsdTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\XsdTable')->getDBTableName());
			
		Application::getConnection(\Xunit\Xmldropshipping\XmlTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\XmlTable')->getDBTableName());

		Application::getConnection(\Xunit\Xmldropshipping\StructureTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\StructureTable')->getDBTableName());
			
		Application::getConnection(\Xunit\Xmldropshipping\PricepropertiesTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\PricepropertiesTable')->getDBTableName());
			
		Application::getConnection(\Xunit\Xmldropshipping\SettingsPriceListImportTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\SettingsPriceListImportTable')->getDBTableName());

		Application::getConnection(\Xunit\Xmldropshipping\ReportParsingTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\ReportParsingTable')->getDBTableName());
		
		/*
		Application::getConnection(\Xunit\Xmldropshipping\ReportParsingExtendTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\ReportParsingExtendTable')->getDBTableName());
		*/
		
		Application::getConnection(\Xunit\Xmldropshipping\TemporyProductTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\TemporyProductTable')->getDBTableName());

		Application::getConnection(\Xunit\Xmldropshipping\LogParsingTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\LogParsingTable')->getDBTableName());
			
		Application::getConnection(\Xunit\Xmldropshipping\MainReportTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\MainReportTable')->getDBTableName());

		Application::getConnection(\Xunit\Xmldropshipping\StepsTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\StepsTable')->getDBTableName());
			
		Application::getConnection(\Xunit\Xmldropshipping\ProductsTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\ProductsTable')->getDBTableName());
			
		Application::getConnection(\Xunit\Xmldropshipping\ComparePropertiesTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\ComparePropertiesTable')->getDBTableName());
			
		Application::getConnection(\Xunit\Xmldropshipping\AutoUpdatePriceListTable::getConnectionName())->
            queryExecute('drop table if exists '.Base::getInstance('\Xunit\Xmldropshipping\AutoUpdatePriceListTable')->getDBTableName());		
		
        Option::delete($this->MODULE_ID);
		
    }

	public function GetPath($notDocumentRoot = false){
		
		if($notDocumentRoot){
			
			return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
			
		}else{
			
			return dirname(__DIR__);
			
		}
		
	} 

	function UnInstallFiles(){
		
		$path = $this->GetPath();
		
		if(\Bitrix\Main\IO\Directory::isDirectoryExists($path.'/install/themes')){

			\Bitrix\Main\IO\Directory::deleteDirectory($_SERVER['DOCUMENT_ROOT'].'/bitrix/themes/.default/icons/'.$this->MODULE_ID);
			\Bitrix\Main\IO\File::deleteFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/themes/.default/'.$this->MODULE_ID.'.css');
			
        }
		
        if(\Bitrix\Main\IO\Directory::isDirectoryExists($path.'/admin')){
			
			\Bitrix\Main\IO\File::deleteFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/sellers.php');
			\Bitrix\Main\IO\File::deleteFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/sellers_edit.php');
			\Bitrix\Main\IO\File::deleteFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/sellers_edit_old.php');
            
        }
		
		return true;
		
	}

	function InstallFiles($arParams = array()){

		$path = $this->GetPath();
		
		if(\Bitrix\Main\IO\Directory::isDirectoryExists($path.'/install/themes')){

            $copyThemes = CopyDirFiles($path."/install/themes", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes", false, true);
			
        }
		
        if(\Bitrix\Main\IO\Directory::isDirectoryExists($path.'/admin')){
			
            CopyDirFiles($path."/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
			
        }

        return true;
		
	}

	public function isVersionD7(){
		
		return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
		
	}

    function DoInstall(){

        global $APPLICATION;

        if($this->isVersionD7()){

			\Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
			
			$this->InstallFiles();
            $this->InstallDB();
            //$this->InstallEvents();
            

        }else{

            $APPLICATION->ThrowException(Loc::getMessage("XUNIT_XMLDROPSHIPPING_INSTALL_ERROR_VERSION"));

        }

        $APPLICATION->includeAdminFile(Loc::getMessage("XUNIT_XMLDROPSHIPPING_INSTALL_TITLE"), $this->GetPath()."/install/step.php");

    }

    function DoUninstall(){

		global $APPLICATION;

        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();

		if($request["step"] < 2){
			
			$APPLICATION->includeAdminFile(Loc::getMessage("XUNIT_XMLDROPSHIPPING_UNINSTALL_TITLE"), $this->GetPath()."/install/unstep1.php");
			
		}elseif($request["step"] == 2){
			
			//$this->UnInstallEvents();
			$this->UnInstallFiles();

			if($request["savedata"] != "Y"){

				$this->UnInstallDB();

			}

			$resultSkladSeller = [];

			$dbResult = CCatalogStore::GetList(
				['ID' => 'ASC'],
				[],
				false,
				false,
				["ID", "XML_ID"]
			);
							
			while($sklad = $dbResult->GetNext()){			

				$issetSklad = preg_match("/^SELLER_ID_/", $sklad["XML_ID"]);

				if($issetSklad){
						
					$resultSkladSeller[$sklad["ID"]] = $sklad;
						
				}

			}

			foreach($resultSkladSeller as $sklad){
					
				CCatalogStore::Delete($sklad["ID"]);
					
			}
				
			unset($resultSkladSeller);

			\Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);
			 
			$APPLICATION->IncludeAdminFile(Loc::getMessage("XUNIT_XMLDROPSHIPPING_UNINSTALL_TITLE"), $this->GetPath()."/install/unstep2.php"); 
			
		}

    }
	
	function GetModuleRightList(){
		
        return array(
            "reference_id" 	=> array("D", "K", "S", "W"),
            "reference" 	=> array(
                "[D] ".Loc::getMessage("XUNIT_XMLDROPSHIPPING_DENIED"),
                "[K] ".Loc::getMessage("XUNIT_XMLDROPSHIPPING_COMPONENT"),
                "[S] ".Loc::getMessage("XUNIT_XMLDROPSHIPPING_WRITE_SETTINGS"),
                "[W] ".Loc::getMessage("XUNIT_XMLDROPSHIPPING_FULL"))
        );
		
    }

}