<?

class Parsing{
	
	public function getInfoPriceList($priceListId){
		
		$getInfoPriceList = \Xunit\Xmldropshipping\XmlTable::getById($priceListId);

		$result = $getInfoPriceList->fetch();
		
		return $result;

	}
	
	public function getDopProperty($idIblock, $codeProperty){
		
		$getPropertyInfo = CIBlock::GetProperties($idIblock, [], ["CODE" => $codeProperty]);
		$propertyInfo = $getPropertyInfo->Fetch();
		
		return $propertyInfo;
		
	}
	
	public function getPropertiesValue($iblockId, $idElement, $code = "", $multiple = "N"){
		
		$result = [];
		
		$db_props = CIBlockElement::GetProperty($iblockId, $idElement, ["sort" => "asc"], ["CODE" => $code]);
		
		if($multiple == "N"){
		
			$result = $db_props->Fetch();
			
		}else{
			
			while($ar_props = $db_props->Fetch()){
				
				$result[] = $ar_props;
				
			}
			
		}	
			
		return $result;
		
	}
	
	public function getInfoElement($idElement){
		
		$res = CIBlockElement::GetByID($idElement);
		$ar_res = $res->GetNext();
		
		return $ar_res;
		
	}

	public function getStopParsing($priceListId, $lastIdImport){
		
		global $DB;
		  
			$resultSelectCancelPriceList = $DB->Query("SELECT 
				`ID_PRICE_LIST`,
				`LAST_ID_IMPORT`,
				`STEP`
			FROM
				`sellers_import_xunit_xml_report_parsing`
			WHERE
				`ID_PRICE_LIST` = $priceListId AND 
				`LAST_ID_IMPORT` = $lastIdImport AND
				`CANCEL_IMPORT` = 'Y' 
			");
		
		$statusSelectCancelPriceList = $resultSelectCancelPriceList->fetch();
		
		return $statusSelectCancelPriceList;
		
	}

	public function deleteMorePhoto($idElement, $idIblock){
		
		CIBlockElement::SetPropertyValuesEx($idElement, $idIblock, ["MORE_PHOTO" => ["VALUE" => ["del" => "Y"]]]);
		
	}
	
	public function getStoreProductInfo($idProduct, $storeId){
		
		$rsStore = CCatalogStoreProduct::GetList([], ['PRODUCT_ID' => $idProduct, 'STORE_ID' => $storeId], false, false); 
		$arStore = $rsStore->Fetch();
		
		return $arStore;
		
	}
	
	public function getStorageInfo($idSeller){

		$dbResult = CCatalogStore::GetList(
			[],
			['XML_ID' => "SELLER_ID_$idSeller"],
			false,
			false,
			["ID"]
		);

		$getIdSklad = $dbResult->Fetch();
		
		return $getIdSklad;
		
	}
	
	public function getSettingsPriceList($priceListId){
		
		$getSettings = \Xunit\Xmldropshipping\SettingsPriceListImportTable::getList([
			'select'  => [
				"ID",
				"ID_XML",
				"SETTINGS_PRICELIST"
			],
			'filter'  => [
				"=ID_XML" => $priceListId
			]
		]);

		$settings = $getSettings->fetch();
		
		$settings = json_decode($settings["SETTINGS_PRICELIST"], true);
		
		return $settings;

	}
	
	public function getCheckedStructure($priceListId){
		
		$getCheckedStructure = \Xunit\Xmldropshipping\StructureTable::getList([
			'select'  => [
				"ID_XML",
				"ID_CATEGORY_XML",
				"ID_CATEGORY_SITE"
			],
			'filter'  => [
				"=ID_XML" => $priceListId
			]
		]);

		$checkedStructure = $getCheckedStructure->fetchAll();
					
		$checkedStructureCategoryXmlArray = [];
					
		foreach($checkedStructure as $structure){
						
			$checkedStructureCategoryXmlArray[$structure["ID_CATEGORY_XML"]] = $structure["ID_CATEGORY_SITE"];
						
		}
		
		return $checkedStructureCategoryXmlArray;
		
	}
	
	public function getCheckedProperties($priceListId){
		
		$getCheckedProperties = \Xunit\Xmldropshipping\PricepropertiesTable::getList([
			'select'  => [
				"ID_XML",
				"ID_IBLOCK",
				"TYPE_IBLOCK",
				"NAME_PROPERTIES",
				"VALUE_PROPERTIES"
			],
			'filter'  => [
				"=ID_XML" => $priceListId
			]
		]);

		$checkedProperties = $getCheckedProperties->fetchAll();
		
		$properties = [];
		
		foreach($checkedProperties as $property){
			
			$properties[$property["NAME_PROPERTIES"]] = $property;
			
		}
		
		return $properties;
		
	}
	
	public function getOfferInfo($priceListId, $lastIdImport){
		
		$getOfferInfo = \Xunit\Xmldropshipping\TemporyProductTable::getList([
			'select'  => [
				"ID",
				"LAST_ID_IMPORT",
				"ID_PRICE_LIST",
				"PRODUCT",
				"ID_SELLER"
			],
			'filter'  => [
				"=ID_PRICE_LIST" 	=> $priceListId,
				"=LAST_ID_IMPORT" 	=> $lastIdImport
			],
			'limit' => 20
		]);
					
		$offerInfo = $getOfferInfo->fetchAll();
		
		return $offerInfo;
		
	}
	
	public function getOfferInSite($offerIdInXml, $idSeller, $idIblock){
		
		$getResultIb = CCatalogSKU::GetInfoByProductIBlock($idIblock);

		if(is_array($getResultIb)){
		
			$arSelect = [
				"ID",
				"IBLOCK_ID",
				"PROPERTY_ID_OFFER_SELLER",
				"PROPERTY_ID_SELLER"
			];
			
			$arFilter = [
				"IBLOCK_ID" 				=> $getResultIb['IBLOCK_ID'],
				"PROPERTY_ID_OFFER_SELLER" 	=> $offerIdInXml,
				"PROPERTY_ID_SELLER" 		=> $idSeller
			];
			
			$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);
			$ob = $res->GetNext();
			
			if(empty($ob)){
					
				return "0";
					
			}else{
					
				return "1";
					
			}
			
		}else{
			
			return "2";
			
		}
		
	}
	
	public function getOfferInfoInSite($offerIdInXml, $idSeller, $idIblock){
		
		$result = [];
		
		$getResultIb = CCatalogSKU::GetInfoByProductIBlock($idIblock);
		
		if(is_array($getResultIb)){
		
			$arSelectOffer = [
				"ID",
				"IBLOCK_ID",
				"PROPERTY_ID_OFFER_SELLER",
				"PROPERTY_ID_SELLER"
			];
			
			$arFilterOffer = [
				"IBLOCK_ID" 				=> $getResultIb['IBLOCK_ID'],
				"PROPERTY_ID_OFFER_SELLER" 	=> $offerIdInXml,
				"PROPERTY_ID_SELLER" 		=> $idSeller
			];
			
			$resOffer = CIBlockElement::GetList([], $arFilterOffer, false, false, $arSelectOffer);
			$obOffer = $resOffer->GetNext();
			
			if(!empty($obOffer)){
				
				$mxResult = CCatalogSku::GetProductInfo($obOffer["ID"]);
				
				if(is_array($mxResult)){
					
					$result = [
						"ID_PRODUCT" 	=> $mxResult['ID'],
						"ID_OFFER"		=> $obOffer["ID"]
					];
					
				}
				
			}
			
		}else{
			
			return false;
			
		}
		
		return $result;
		
	}
	
	public function getPropertiesValueProductAndOffer($idElement, $stores){
		
		$arSelect = ["ID", "IBLOCK_ID", "PROPERTY_*"];
		$arFilter = ["ID" => $idElement];

		$res = CIBlockElement::GetList([], $arFilter, false, false, $arSelect);

		$ob = $res->GetNextElement();

		$arProps = $ob->GetProperties();

		$result = [];
		
		$excludeArray = [];
		$excludeArray[] = "MORE_PHOTO";
		
		foreach($stores as $store){

			$excludeArray[] = "NAME_".$store["UF_XML_ID"];
			$excludeArray[] = "DONT_EXPORT_".$store["UF_XML_ID"];
			
		}

		foreach($arProps as $keyProp => $prop){
			
			if(!in_array($keyProp, $excludeArray)){
			
				switch($prop["PROPERTY_TYPE"]){
					
					case "S":
						
						switch($prop["LIST_TYPE"]){
					
							case "L":
								
								switch($prop["USER_TYPE"]){
					
									case "directory":
										
										switch($prop["MULTIPLE"]){
					
											case "N":
											case "Y":
												
												$result[$prop["ID"]] = $prop["VALUE"];
												
											break;
											
										}
										
									break;
									
									case "":
											
										switch($prop["MULTIPLE"]){

											case "Y":
											case "N":

												$result[$prop["ID"]] = $prop["VALUE"];

											break;
													
										}	

									break;
									
								}
								
							break;
							
						}
						
					break;
					
					case "L":
						
						switch($prop["LIST_TYPE"]){
					
							case "L":
								
								switch($prop["USER_TYPE"]){
					
									case "":
										
										switch($prop["MULTIPLE"]){
					
											case "N":
											case "Y":
												
												$result[$prop["ID"]] = $prop["VALUE_ENUM_ID"];
												
											break;
											
										}
										
									break;

								}
								
							break;
							
						}
						
					break;
					
					case "E":
						
						switch($prop["LIST_TYPE"]){
					
							case "L":
								
								switch($prop["USER_TYPE"]){
					
									case "SKU":
										
										switch($prop["MULTIPLE"]){
					
											case "N":
												
												$result[$prop["ID"]] = $prop["VALUE"];
												
											break;
											
										}
										
									break;

								}
								
							break;
							
						}
						
					break;
					
				}
			
			}
			
		}
		
		return $result;
		
	}
	
	public function getDelimeterInPropertiesValue($delimeter, $propertiesValue){
		
		$pos = strpos($propertiesValue, $delimeter);

		if($pos === false){
			
			$result = $propertiesValue;
			
		}else{
			
			$result = explode($delimeter, $propertiesValue);
			
		}
		
		return $result;
		
	}
	
	public function getResultProperties($propertyInfo, $valueProperty, $settingsDelimeter){
		
		$getDelimeterInPropertiesValue = self::getDelimeterInPropertiesValue($settingsDelimeter, $valueProperty);
	
		$properties = [];

			switch($propertyInfo["PROPERTY_TYPE"]){

                case "S":

                    switch($propertyInfo["LIST_TYPE"]){

                        case "L":

                            switch($propertyInfo["USER_TYPE"]){
									
                                case "directory":
									
									$getHighloadId = exportXml::getHighloadId($propertyInfo["USER_TYPE_SETTINGS"]["TABLE_NAME"]);

                                    switch($propertyInfo["MULTIPLE"]){

                                        case "N":

											if(!is_array($getDelimeterInPropertiesValue)){

												$getElementByHighload = exportXml::getHigloadInfo(
													$getHighloadId, 
													"ONE_ELEMENT", 
													$select = ['UF_NAME', 'UF_XML_ID'], 
													$order = ['ID' => 'ASC'], 
													$limit = false, 
													["UF_NAME" => $valueProperty]
												);
													
												if(empty($getElementByHighload)){
														
													$newElementInHigload = exportXml::addElementToHighload($getHighloadId, $valueProperty);
														
													$properties = $newElementInHigload;
														
												}else{
														
													$properties = $getElementByHighload["UF_XML_ID"];
														
												}
											
											}else{
												
												foreach($getDelimeterInPropertiesValue as $valueProperty){
													
													$valueProperty = trim($valueProperty);
													
													$getElementByHighload = exportXml::getHigloadInfo(
														$getHighloadId, 
														"ONE_ELEMENT", 
														$select = ['UF_NAME', 'UF_XML_ID'], 
														$order = ['ID' => 'ASC'], 
														$limit = false, 
														["UF_NAME" => $valueProperty]
													);
														
													if(empty($getElementByHighload)){
															
														$newElementInHigload = exportXml::addElementToHighload($getHighloadId, $valueProperty);
															
														$properties[] = $newElementInHigload;
															
													}else{
															
														$properties[] = $getElementByHighload["UF_XML_ID"];
															
													}
												
												}
												
											}

                                        break;
										
										case "Y":
											
											if(is_array($valueProperty)){
											
												foreach($valueProperty as $value){
												
													$getElementByHighload = exportXml::getHigloadInfo(
														$getHighloadId, 
														"ONE_ELEMENT", 
														$select = ['UF_NAME', 'UF_XML_ID'], 
														$order = ['ID' => 'ASC'], 
														$limit = false, 
														["UF_NAME" => $value]
													);
														
													if(empty($getElementByHighload)){
															
														$newElementInHigload = exportXml::addElementToHighload($getHighloadId, $valueProperty);
															
														$properties[] = $newElementInHigload;
															
													}else{
															
														$properties[] = $getElementByHighload["UF_XML_ID"];
															
													}
													
												}
											
											}else{
												
												if(!is_array($getDelimeterInPropertiesValue)){
												
													$getElementByHighload = exportXml::getHigloadInfo(
														$getHighloadId, 
														"ONE_ELEMENT", 
														$select = ['UF_NAME', 'UF_XML_ID'], 
														$order = ['ID' => 'ASC'], 
														$limit = false, 
														["UF_NAME" => $valueProperty]
													);
														
													if(empty($getElementByHighload)){
															
														$newElementInHigload = exportXml::addElementToHighload($getHighloadId, $valueProperty);
															
														$properties = $newElementInHigload;
															
													}else{
															
														$properties = $getElementByHighload["UF_XML_ID"];
															
													}
												
												}else{
													
													foreach($getDelimeterInPropertiesValue as $valueProperty){
													
														$valueProperty = trim($valueProperty);
													
														$getElementByHighload = exportXml::getHigloadInfo(
															$getHighloadId, 
															"ONE_ELEMENT", 
															$select = ['UF_NAME', 'UF_XML_ID'], 
															$order = ['ID' => 'ASC'], 
															$limit = false, 
															["UF_NAME" => $valueProperty]
														);
															
														if(empty($getElementByHighload)){
																
															$newElementInHigload = exportXml::addElementToHighload($getHighloadId, $valueProperty);
																
															$properties[] = $newElementInHigload;
																
														}else{
																
															$properties[] = $getElementByHighload["UF_XML_ID"];
																
														}
													
													}
													
												}
												
											}
											
										break;

                                    }
										
								break;

                                default:
								
									switch($propertyInfo["MULTIPLE"]){

                                        case "Y":
											
											//$properties = $valueProperty;
											
										break;
										
									}	

                                break;

                            }

                        break;

                    }

                break;

                case "N":

                        switch($ar_props["LIST_TYPE"]){

                            case "L":

                                switch($ar_props["USER_TYPE"]){

                                    default:

                                        switch($ar_props["MULTIPLE"]){

                                            case "N":
/*
                                                $ar_props = $db_props->GetNext();

                                                $props[$ar_props["CODE"]] = [
                                                    "NAME" 		=> $ar_props["NAME"],
                                                    "VALUE" 	=> $ar_props["VALUE"],
													"PARAMS"	=> $propertyParams
                                                ];
*/
                                            break;

                                        }

                                    break;

                                }

                            break;

                        }

                    break;

                    case "F":

                        switch($ar_props["LIST_TYPE"]){

                            case "L":

                                switch($ar_props["USER_TYPE"]){

                                    default:

                                        switch($ar_props["MULTIPLE"]){

                                            case "Y":
/*
                                                 while($ar_props = $db_props->GetNext()){

                                                    if(!empty($ar_props["VALUE"])){

                                                        $getFile = CFile::ResizeImageGet(
                                                            $ar_props["VALUE"],
                                                            array("width" => 1920, "height" => 1280),
                                                            BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
                                                            true
                                                        );

                                                        $props[$ar_props["CODE"]]["NAME"] = $ar_props["NAME"];
                                                        $props[$ar_props["CODE"]]["VALUE"][] = "https://".$_SERVER["SERVER_NAME"].$getFile["src"];
														$props[$ar_props["CODE"]]["PARAMS"] = $propertyParams;
														
                                                    }

                                                }
*/
                                            break;

                                        }

                                    break;

                                }

                            break;

                        }

                    break;

                    case "L":

                        switch($ar_props["LIST_TYPE"]){

                            case "C":

                                switch($ar_props["USER_TYPE"]){

                                    default:

                                        switch($ar_props["MULTIPLE"]){

                                            case "N":
/*
                                                $ar_props = $db_props->GetNext();

                                                $props[$ar_props["CODE"]] = [
                                                    "NAME" 		=> $ar_props["NAME"],
                                                    "VALUE" 	=> $ar_props["VALUE"],
													"PARAMS"	=> $propertyParams
                                                ];
*/
                                            break;

                                        }

                                    break;

                                }

                            break;

                            case "L":

                                switch($ar_props["USER_TYPE"]){

                                    default:

                                        switch($ar_props["MULTIPLE"]){

                                            case "N":
/*
                                                $ar_props = $db_props->GetNext();

                                                $ar_props_value = CIBlockFormatProperties::GetDisplayValue(
                                                    ["ID" => $idProduct],
                                                    $ar_props,
                                                    ""
                                                );

                                                $props[$ar_props["CODE"]] = [
                                                    "NAME" 		=> $ar_props["NAME"],
                                                    "VALUE" 	=> $ar_props_value["VALUE_ENUM"],
													"PARAMS"	=> $propertyParams
                                                ];
*/
                                            break;

                                        }

                                    break;

                                }

                            break;

                        }

                    break;

                }

		return $properties;
		
	}
	
}