<?
use \Bitrix\Main\Loader;
	
$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/ext_www/crocodeal.com.ua";
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);

class Cron{
	
	public static $idSeller;
	public static $idPriceList;
	
	public function worker($idSeller, $idPriceList){
		
		self::$idSeller = $idSeller;
		self::$idPriceList = $idPriceList;
		
		\Bitrix\Main\Loader::registerAutoLoadClasses(
			null,
			["dumpingCUrl" => "/local/lib/php/curl.php"]
		);

		$xmlInfo = self::getXmlInfo();
			
		$getTimeTableSettings = \Xunit\Xmldropshipping\AutoUpdatePriceListTable::getList(
			[
				"select" => [
					"ID",
					"ACTIVE",
					"ID_AGENT",
					"TIME_SETTING"
				],
				"filter" => [
					"ID_SELLER" => self::$idSeller,
					"ID_XML" 	=> self::$idPriceList
				]
			]
		);
			
		$getTimeTableSettings = $getTimeTableSettings->fetch();
			
		$curl = new dumpingCUrl($xmlInfo["FILE_XML_LINK_SELLER_LINK"]);
		
		$errors = [];
							
		$fileInfo = $curl->getFileInfo();
							
		AddMessage2Log(print_r($fileInfo, 1), "fileInfo");
						
		if($fileInfo["http_code"] != 206 && $fileInfo["http_code"] != 200){
								
			$errors[1] = "Код ответа от сервера поставщика = ".$fileInfo["http_code"];
								
		}
							
		if($fileInfo["content_type"] != "application/xml" && $fileInfo["content_type"] != "text/xml; charset=UTF-8" && $fileInfo["content_type"] != "text/xml"){
								
			$errors[2] = "Файл поставщика не является XML файлом.";
								
		}
							
		if($fileInfo["download_content_length"] <= 0 || $fileInfo["file_size"] <= 0){
								
			$errors[3] = "Файл поставщика пустой.";
								
		}
							
		if($fileInfo["accept-ranges"] != "bytes"){

			$errors[4] = "Скачивание проихсодить не будет, т.к. я специально при отсутствии bytes не даю разрешение на продолжение скачивания. Полный игнор. Полная остановка.";

		}
		
		AddMessage2Log(print_r($errors, 1), "BEFORE_DOWNLOAD_ERRORS");

		if(empty($errors)){
			
			$downloadFile = $curl->downloadFileAll($_SERVER["DOCUMENT_ROOT"].$xmlInfo["FILE_XML_LINK_SELLER"], $xmlInfo["FILE_XML_LINK_SELLER_LINK"]);
									
			if($downloadFile){
										
				$fileSizeInServer = filesize($_SERVER["DOCUMENT_ROOT"].$xmlInfo["FILE_XML_LINK_SELLER"]);
				$fileSizeInServerManufacture = $fileInfo["download_content_length"];
				$fileSizeInServerManufacture2 = $fileInfo["file_size"];
										
				if($fileSizeInServer == $fileSizeInServerManufacture || $fileSizeInServer == $fileSizeInServerManufacture2){
											
					if($getTimeTableSettings["ACTIVE"] == "Y"){
												
						$getSettingsPriceList = \Xunit\Xmldropshipping\ReportParsingTable::getList([
							'select'  => [
								"ID_PRICE_LIST",
								"ID_SELLER",
								"LAST_ID_IMPORT"
							],
							'filter'  => [
								"ID_PRICE_LIST" => self::$idPriceList,
								"ID_SELLER" 	=> self::$idSeller
							],
							'order'	  => [
								"LAST_ID_IMPORT" => "DESC"
							],
							'limit'   => 1
						]);

						$resultGetSettingsPriceList = $getSettingsPriceList->fetch();
												
						if(empty($resultGetSettingsPriceList)){
												
							$lastIdImport = 1;
													
						}else{
													
							$lastIdImport = $resultGetSettingsPriceList["LAST_ID_IMPORT"] + 1;
													
						}
												
						$params = [];
											
						$params["STEP_IMPORT"] = 0;
						$params["ID_PRICELIST"] = self::$idPriceList;
						$params["ID_SELLER"] = self::$idSeller;
						$params["LAST_ID_IMPORT"] = $lastIdImport;
												
						$rab = new rabbit();
						$rab->rabbitNewConnection();
						$rab->rabbitNewChanel();
						$rab->rabbitNewExchange();
						$rab->rabbitSetExchange("parsing", AMQP_EX_TYPE_DIRECT, AMQP_DURABLE);
						$rab->rabbitNewQueue();
													
						$rab->rabbitSetQueue("parsing", AMQP_IFUNUSED | AMQP_AUTODELETE, "parsing");
						$rab->rabbitSendToExchange($params, "parsing");
												
						$rab->rabbitCloseChanel();
						$rab->rabbitDisconnect();
												
						return "Cron::worker(".self::$idSeller.", ".self::$idPriceList.");";

					}

				}

			}
			
		}else{
			
			AddMessage2Log(print_r($errors, 1), "ERRORS");
			
		}
		
	}
	
	public function getXmlInfo(){
		
		$xmlGetInfo = \Xunit\Xmldropshipping\XmlTable::getList([
			'select'  => [
				"FILE_XML_LINK_SELLER",
				"FILE_XML_LINK_SELLER_LINK"
			],
			'filter'  => [
				"ID" 		=> self::$idPriceList,
				"ID_SELLER" => self::$idSeller,
			]
		]);

		$xmlGetInfo = $xmlGetInfo->fetch();
		
		return $xmlGetInfo;
		
	}
	
}