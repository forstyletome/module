<?
use \Bitrix\Main\Localization\Loc;

Loc::LoadMessages(__FILE__);

if($APPLICATION->GetGroupRight("form") > "D"){
	
	$itemsMenu = [
		[
			"text" 		=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_MENU_1"),
			"url" 		=> "sellers.php?lang=".LANGUAGE_ID,
			"title"		=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_MENU_1"),
			"more_url" 	=> [
				"sellers.php"
			],
		]
	];
	
	$aMenu = array(
		"url" 			=> "sellers.php?lang=".LANGUAGE_ID,
		"parent_menu" 	=> "global_menu_content",
		"sort"        	=> 300,
		"text"        	=> "Импорт прайс-листов поставщиков",
		"icon"        	=> "xunit_xmldropshipping_icon", // малая иконка
		"page_icon"   	=> "xunit_xmldropshipping_page_icon", // большая иконка
		"items_id"    	=> "xunit_xmldropshipping_main",  // идентификатор ветви
		"more_url" 		=> [
			"sellers.php",
			"sellers_edit.php"
		],
	);

	return $aMenu;
	
}

return false;