<?
	use \Bitrix\Main\Localization\Loc as Loc;
	use \Bitrix\Main\Loader;
	use \Bitrix\Main;
	use \Bitrix\Main\Application;
	use \Bitrix\Main\Entity\Base;

	require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
	
	// Наименование модуля
	$module_id = 'xunit.xmldropshipping';
	
	// Подключаем модуль
	Loader::includeModule($module_id);
	Loader::includeModule('iblock');
	Loader::includeModule('catalog');
	
	Loader::registerAutoLoadClasses(
		null,
		[
			"rabbit" 	=> "/local/lib/php/rabbit.php",
			"exportXml" => "/local/lib/php/exportXml.php"
		]
	);

	$request = Application::getInstance()->getContext()->getRequest(); 
	
	if($request->isAjaxRequest()){
		
		header('Content-type: application/json');
		$APPLICATION->RestartBuffer();
		
		$data = $request->get("data");
		
			switch($request->get("type")){
				
				// Сохранение информации о поставщике
				case "saveInfoSeller":
					
					$errors = [];
					
					if(empty($data["NAME_SELLER"])){
					
						$errors[] = "Укажите наименование поставщика";
					
					}
					
					if(empty($errors)){
					
						if(empty($data["ID_SELLER"])){
							
							$res = \Xunit\Xmldropshipping\SellersTable::add(
								[
									"NAME_SELLER"			=> $data["NAME_SELLER"],
									"FIO_SELLER"			=> $data["FIO_SELLER"],
									"SITE_SELLER"			=> $data["SITE_SELLER"],
									"TELEPHONE_SELLER"		=> $data["TELEPHONE_SELLER"],
									"DESCRIPTION_SELLER"	=> $data["DESCRIPTION_SELLER"]
								]
							);
						
							if($result = $res->isSuccess()){
								
								$lastId = $res->getId();
								
								$mainReportAdd_ADD_SELLER = \Xunit\Xmldropshipping\MainReportTable::add(
									[
										"ID_SELLER" 			=> $lastId,
										"STEP" 					=> 0,
										"TASK" 					=> "ADD_SELLER",
										"STATUS" 				=> "SUCCESS",
										"COMMENTS_OR_ERRORS" 	=> "Поставщик успешно добавлен"
									]
								);
								
								$arFields = [
									"TITLE" 		=> "Склад поставщика ID #$lastId ".$data["NAME_SELLER"],
									"ACTIVE" 		=> "Y",
									"ADDRESS" 		=> "Укажите адрес",
									"XML_ID" 		=> "SELLER_ID_$lastId",
								];
								
								if(CCatalogStore::Add($arFields) > 0){
									
									$mainReportAdd_ADD_SELLER_SKLAD = \Xunit\Xmldropshipping\MainReportTable::add(
										[
											"ID_SELLER" 			=> $lastId,
											"STEP" 					=> 0,
											"TASK" 					=> "ADD_SELLER_SKLAD",
											"STATUS" 				=> "SUCCESS",
											"COMMENTS_OR_ERRORS" 	=> "Склад поставщика успешно создан"
										]
									);
									
								}else{
									
									$mainReportAdd_ADD_SELLER_SKLAD = \Xunit\Xmldropshipping\MainReportTable::add(
										[
											"ID_SELLER" 			=> $lastId,
											"STEP" 					=> 0,
											"TASK" 					=> "ADD_SELLER_SKLAD",
											"STATUS" 				=> "FATAL_ERROR",
											"COMMENTS_OR_ERRORS" 	=> "Ошибка при создании склада поставщика"
										]
									);
									
								}
								
								if($mainReportAdd_ADD_SELLER->isSuccess() && $mainReportAdd_ADD_SELLER_SKLAD->isSuccess()){
									
									$success = "Y";
									$redirectUrl = "/bitrix/admin/sellers_edit.php?ID=".$lastId."&lang=ru";
									
								}

							}else{
								
								$mainReportAdd_ADD_SELLER = \Xunit\Xmldropshipping\MainReportTable::add(
									[
										"STEP" 					=> 0,
										"TASK" 					=> "ADD_SELLER",
										"STATUS" 				=> "FATAL_ERROR",
										"COMMENTS_OR_ERRORS" 	=> "Непредвиденная ошибка при добавлении поставщика"
									]
								);
								
								$errors[] = "FATAL ERROR - Непредвиденная ошибка при добавлении поставщика.";
								
							}
						
						}else{
							
							$res = \Xunit\Xmldropshipping\SellersTable::update(
								$data["ID_SELLER"],
								[
									"NAME_SELLER"			=> $data["NAME_SELLER"],
									"FIO_SELLER"			=> $data["FIO_SELLER"],
									"SITE_SELLER"			=> $data["SITE_SELLER"],
									"TELEPHONE_SELLER"		=> $data["TELEPHONE_SELLER"],
									"DESCRIPTION_SELLER"	=> $data["DESCRIPTION_SELLER"]
								]
							);
							
							if($result = $res->isSuccess()){
								
								$mainReportAdd_EDIT_SELLER = \Xunit\Xmldropshipping\MainReportTable::add(
									[
										"ID_SELLER" 			=> $data["ID_SELLER"],
										"STEP" 					=> 0,
										"TASK" 					=> "EDIT_SELLER",
										"STATUS" 				=> "SUCCESS",
										"COMMENTS_OR_ERRORS" 	=> "Информация о поставщике успешно обновлена"
									]
								);
								
								if($mainReportAdd_EDIT_SELLER->isSuccess()){
								
									$success = "Y";
									$redirectUrl = "/bitrix/admin/sellers_edit.php?ID=".$data["ID_SELLER"]."&lang=ru";
								
								}
								
							}else{
								
								$errors[] = "FATAL ERROR - Непредвиденная ошибка при обновлении поставщика.";
								
							}
							
						}
					
					}
				
					echo json_encode([
						"RESULT" => [
							"RESULT" => [
								"SUCCESS" 		=> $success,
								"URL_REDIRECT" 	=> $redirectUrl
							],
							"ERRORS" => $errors
						]
					]);
					
				break;
				// Окончание Сохранение информации о поставщике
				
				// Сохранение XSD файла
				case "saveXsdSeller":
					
					$items = $data["INPUT_XSD"];
					
					$errors = [];
					
					$i = 1;
					
					$xsdData = [];
					
					foreach($items as $item){
						
						if(!empty($item["NAME_PRICE_XSD"]) && empty($item["FILE_PRICE_XSD"])){
							
							$errors[] = "Ошибка добавления XSD схемы #$i. Не выбрана XSD схема.";
							
						}elseif(empty($item["NAME_PRICE_XSD"]) && !empty($item["FILE_PRICE_XSD"])){
							
							$errors[] = "Ошибка добавления XSD схемы #$i. Не указано название XSD схемы.";
							
						}elseif(!empty($item["NAME_PRICE_XSD"]) && !empty($item["FILE_PRICE_XSD"])){
							
							$xsdData[] = $item;
							
						}
						
						$i++;
						
					}
					
					if(empty($xsdData)){
						
						$errors[] = "Ошибка добавления XSD схемы. Нет данных для добавления XSD схемы.";
						
					}
					
					if(empty($errors) && !empty($xsdData)){
						
						foreach($xsdData as $item){
							
							$res = \Xunit\Xmldropshipping\XsdTable::add(
								[
									"ID_SELLER"				=> $data["ID_SELLER"],
									"FILE_XSD_NAME_SELLER"	=> $item["NAME_PRICE_XSD"],
									"FILE_XSD_LINK_SELLER"	=> $item["FILE_PRICE_XSD"]
								]
							);
									
							if(!$res->isSuccess()){
								
								$errors[] = "Критическая ошибка при добавлении XSD схем.";
								
								$mainReportAdd_ADD_XSD_SCHEME = \Xunit\Xmldropshipping\MainReportTable::add(
									[
										"ID_SELLER" 			=> $data["ID_SELLER"],
										"STEP" 					=> 0,
										"TASK" 					=> "ADD_XSD_SCHEME",
										"STATUS" 				=> "FATAL_ERROR",
										"COMMENTS_OR_ERRORS" 	=> "Критическая ошибка при добавлении XSD схем."
									]
								);
								
							}else{
								
								$lastId = $res->getId();
								
								$mainReportAdd_ADD_XSD_SCHEME = \Xunit\Xmldropshipping\MainReportTable::add(
									[
										"ID_SELLER" 			=> $data["ID_SELLER"],
										"ID_XSD" 				=> $lastId,
										"STEP" 					=> 0,
										"TASK" 					=> "ADD_XSD_SCHEME",
										"STATUS" 				=> "SUCCESS",
										"COMMENTS_OR_ERRORS" 	=> "XSD схема успешно добавлена"
									]
								);
								
							}
							
						}
						
					}
					
					if(empty($errors)){
						
						$result = "Y";
						
					}
					
					echo json_encode([
						"RESULT" => [
							"RESULT" => $result,
							"ERRORS" => $errors
						]
					]);
					
				break;
				// Окончание сохранения XSD файла
				
				// Сохранение прайс-листа
				case "saveXmlSeller":
					
					$items = $data["INPUT_XML"];
					
					$errors = [];
					
					$i = 1;
					
					$xsdData = [];
					
					foreach($items as $item){
						
						if(!empty($item["NAME_PRICE_XML"]) && empty($item["FILE_PRICE_XML"])){
							
							$errors[] = "Ошибка добавления прайс-листа #$i. Не выбран прайс-лист.";
							
						}elseif(empty($item["NAME_PRICE_XML"]) && !empty($item["FILE_PRICE_XML"])){
							
							$errors[] = "Ошибка добавления прайс-листа #$i. Не указано название прайс-листа.";
							
						}elseif(empty($item["FILE_PRICE_XML_LINK"])){
							
							$errors[] = "Ошибка добавления прайс-листа #$i. Не указан URL прайс-листа поставщика.";
							
						}elseif(empty($item["XSD_SCHEME"])){
							
							$errors[] = "Ошибка добавления прайс-листа #$i. Не указана XSD схема.";
							
						}elseif(!empty($item["NAME_PRICE_XML"]) && !empty($item["FILE_PRICE_XML"]) && !empty($item["XSD_SCHEME"])){
							
							$xmlData[] = $item;
							
						}
						
						$i++;
						
					}
					
					if(empty($xmlData)){
						
						$errors[] = "Ошибка добавления прайс-листа. Нет данных для добавления прайс-листа.";
						
					}
					
					if(empty($errors) && !empty($xmlData)){
						
						foreach($xmlData as $item){
							
							$res = \Xunit\Xmldropshipping\XmlTable::add(
								[
									"ID_SELLER"						=> $data["ID_SELLER"],
									"ID_XSD"						=> $item["XSD_SCHEME"],
									"FILE_XML_NAME_SELLER"			=> $item["NAME_PRICE_XML"],
									"FILE_XML_LINK_SELLER"			=> $item["FILE_PRICE_XML"],
									"FILE_XML_LINK_SELLER_LINK"		=> $item["FILE_PRICE_XML_LINK"],
								]
							);
									
							if(!$res->isSuccess()){
								
								$mainReportAdd_ADD_XML_PRICE = \Xunit\Xmldropshipping\MainReportTable::add(
									[
										"ID_SELLER" 			=> $data["ID_SELLER"],
										"ID_XSD" 				=> $item["XSD_SCHEME"],
										"STEP" 					=> 0,
										"TASK" 					=> "ADD_XML_PRICE",
										"STATUS" 				=> "FATAL_ERROR",
										"COMMENTS_OR_ERRORS" 	=> "Критическая ошибка при добавлении прайс-листа"
									]
								);
								
								$errors[] = "Критическая ошибка при добавлении прайс-листа.";
								
							}else{
								
								$lastId = $res->getId();
								
								$mainReportAdd_ADD_XML_PRICE = \Xunit\Xmldropshipping\MainReportTable::add(
									[
										"ID_SELLER" 			=> $data["ID_SELLER"],
										"ID_XSD" 				=> $item["XSD_SCHEME"],
										"ID_XML" 				=> $lastId,
										"STEP" 					=> 0,
										"TASK" 					=> "ADD_XML_PRICE",
										"STATUS" 				=> "SUCCESS",
										"COMMENTS_OR_ERRORS" 	=> "XML файл с ID ".$lastId." успешно добавлен"
									]
								);
								
								$mainStepsAdd_ADD_STEPS = \Xunit\Xmldropshipping\StepsTable::add(
									[
										"ID_SELLER" 							=> $data["ID_SELLER"],
										"ID_XSD" 								=> $item["XSD_SCHEME"],
										"ID_XML" 								=> $lastId,
										"ISSET_SETTINGS"						=> "N",
										"ANALIZE_PRICE_LIST"					=> "N"
									]
								);
								
							}
							
						}
						
					}
					
					if(empty($errors)){
						
						$result = "Y";
						
					}
					
					echo json_encode([
						"RESULT" => [
							"RESULT" => $result,
							"ERRORS" => $errors
						]
					]);
					
				break;
				// Окончание сохранения прайс-листа
				
				// Удаление XSD файла
				case "deleteXsdSeller":
					
					$errors = [];
					
					if(!empty($data["ID_SELLER"]) && !empty($data["ID_SELLER_XSD"])){
						
						$sellerXsdGetInfo = \Xunit\Xmldropshipping\XsdTable::getList([
							'select'  => ["ID", "FILE_XSD_LINK_SELLER"],
							'filter'  => [
								"=ID" => $data["ID_SELLER_XSD"]
							]
						]);
						
						$sellerXsdInfo = $sellerXsdGetInfo->fetch();
						
						if(!empty($sellerXsdInfo)){
							
							unlink($_SERVER["DOCUMENT_ROOT"].$sellerXsdInfo["FILE_XSD_LINK_SELLER"]);
							
						}
						
						$res = \Xunit\Xmldropshipping\XsdTable::delete($data["ID_SELLER_XSD"]);
						
						if($res->isSuccess()){
							
							$mainReportAdd_DELETE_XSD_FILE = \Xunit\Xmldropshipping\MainReportTable::add(
								[
									"ID_SELLER" 			=> $data["ID_SELLER"],
									"ID_XSD" 				=> $data["ID_SELLER_XSD"],
									"STEP" 					=> 1,
									"TASK" 					=> "DELETE_XSD_SCHEME",
									"STATUS" 				=> "SUCCESS",
									"COMMENTS_OR_ERRORS" 	=> "XSD схема успешно удалена"
								]
							);
							
							if($mainReportAdd_DELETE_XSD_FILE->isSuccess()){
							
								$sellerXmlGetInfo = \Xunit\Xmldropshipping\XmlTable::getList([
									'select'  => ["ID", "FILE_XML_LINK_SELLER", "ID_SELLER"],
									'filter'  => [
										"ID_XSD" => $data["ID_SELLER_XSD"]
									]
								]);
								
								$sellerXmlInfo = $sellerXmlGetInfo->fetchAll();
								
								if(!empty($sellerXmlInfo)){
									
									foreach($sellerXmlInfo as $item){
										
										$params["STEP_IMPORT"] = "DELETE_PRICE_LIST";
										$params["ID_PRICELIST"] = $item["ID"];
										$params["ID_SELLER"] = $item["ID_SELLER"];
											
										/*
										$stepImport
										$priceListId
										$priceListSellerId
										$lastIdImport
										$idListProduct
										$offsetInImport
										$data
										*/
										
										$rab = new rabbit();
										$rab->rabbitNewConnection();
										$rab->rabbitNewChanel();
										$rab->rabbitNewExchange();
										$rab->rabbitSetExchange("parsing", AMQP_EX_TYPE_DIRECT, AMQP_DURABLE);
										$rab->rabbitNewQueue();
											
										$rab->rabbitSetQueue("parsing", AMQP_IFUNUSED | AMQP_AUTODELETE, "parsing");
										$rab->rabbitSendToExchange($params, "parsing");
										
										$rab->rabbitCloseChanel();
										$rab->rabbitDisconnect();
										
									}
								
								}
							
							}
							
							if(empty($errors)){
								
								$result = "SUCCESS";
							
							}
							
						}else{
							
							$mainReportAdd_DELETE_XSD_SCHEME = \Xunit\Xmldropshipping\MainReportTable::add(
								[
									"ID_SELLER" 			=> $data["ID_SELLER"],
									"ID_XSD" 				=> $data["ID_SELLER_XSD"],
									"STEP" 					=> 1,
									"TASK" 					=> "DELETE_XSD_SCHEME",
									"STATUS" 				=> "FATAL_ERROR",
									"COMMENTS_OR_ERRORS" 	=> "Критическая ошибка при удалении XSD схемы"
								]
							);
							
							$errors[] = "Критическая ошибка при удалении XSD схемы.";
							
						}
						
					}else{
						
						$mainReportAdd_DELETE_XSD_SCHEME = \Xunit\Xmldropshipping\MainReportTable::add(
							[
								"ID_SELLER" 			=> $data["ID_SELLER"],
								"ID_XSD" 				=> $data["ID_SELLER_XSD"],
								"STEP" 					=> 1,
								"TASK" 					=> "DELETE_XSD_SCHEME",
								"STATUS" 				=> "FATAL_ERROR",
								"COMMENTS_OR_ERRORS" 	=> "Критическая ошибка. Не хватает данных для удаления XSD схемы"
							]
						);
						
						$errors[] = "Критическая ошибка. Не хватает данных для удаления.";
						
					}
					
					if($result == "SUCCESS" && empty($errors)){
						
						$result = "Y";
						
					}
					
					echo json_encode([
						"RESULT" => [
							"RESULT" => $result,
							"ERRORS" => $errors
						]
					]);
					
				break;
				// Окончание удаления XSD файла
				
				// Удаление прайс-листа
				case "deleteXmlSeller":
					
					$errors = [];
					
					if(!empty($data["ID_SELLER"]) && !empty($data["ID_SELLER_XML"])){
						
						$params["STEP_IMPORT"] = "DELETE_PRICE_LIST";
						$params["ID_PRICELIST"] = $data["ID_SELLER_XML"];
						$params["ID_SELLER"] = $data["ID_SELLER"];
						$params["OFFSET_IN_IMPORT"] = 0;
						
						/*
						$stepImport
						$priceListId
						$priceListSellerId
						$lastIdImport
						$idListProduct
						$offsetInImport
						$data
						*/

						$rab = new rabbit();
						$rab->rabbitNewConnection();
						$rab->rabbitNewChanel();
						$rab->rabbitNewExchange();
						$rab->rabbitSetExchange("parsing", AMQP_EX_TYPE_DIRECT, AMQP_DURABLE);
						$rab->rabbitNewQueue();

						$rab->rabbitSetQueue("parsing", AMQP_IFUNUSED | AMQP_AUTODELETE, "parsing");
						$rab->rabbitSendToExchange($params, "parsing");

						$rab->rabbitCloseChanel();
						$rab->rabbitDisconnect();
						
						$result = "Y";
						
					}else{
						
						$errors[] = "Критическая ошибка. Не хватает данных для удаления.";
						
					}
					
					echo json_encode([
						"RESULT" => [
							"RESULT" => $result,
							"ERRORS" => $errors
						]
					]);
					
				break;
				// Окончание удаления прайс-листа
				
				// Получение ошибок при анализе прайс-листа
				case "getErrorsAnalize":
					
					$errors = [];
					
					$getResultAnalizeFile = \Xunit\Xmldropshipping\MainReportTable::getList(
						[
							"select" => [
								"ID",
								"ID_SELLER", 
								"ID_XML", 
								"STEP", 
								"TASK", 
								"STATUS", 
								"COMMENTS_OR_ERRORS"
							],
							"filter" => [
								"ID_SELLER" => $data["ID_SELLER"],
								"ID_XML" 	=> $data["ID_PRICELIST"],
								"STEP"		=> 1,
								"TASK"		=> "ANALIZE_PRICE_LIST",
								"STATUS"	=> "FATAL_ERROR"
							],
							"order" => [
								"ID" => "DESC"
							],
							"limit" => 1
						]
					);
					
					$getResultAnalizeFile = $getResultAnalizeFile->fetch();
					
					echo json_encode([
						"RESULT" => [
							"RESULT" => $getResultAnalizeFile["COMMENTS_OR_ERRORS"],
							"ERRORS" => $errors
						]
					]);
					
				break;
				// Окончание получения ошибок при анализе прайс-листа
				
				// Получение настроек прайс-листа
				case "getSettingsImportPriceList":
					
					$exportXml = new exportXml();
					
					$errors = [];
					$result = [];
					
					$getStores = $exportXml->getStoresInfo();
					$stores = [];
					$prices = [];
					
					foreach($getStores as $store){
						
						$stores[$store["UF_XML_ID"]] = "N";
						
						$prices[$store["UF_XML_ID"]] = [
							"TYPE" 	=> "1",
							"VALUE"	=> "0"
						];
						
					}
					
					$getSettingsPriceList = \Xunit\Xmldropshipping\SettingsPriceListImportTable::getList([
						'select'  => [
							"SETTINGS_PRICELIST"
						],
						'filter'  => [
							"=ID_XML" => $data["ID_PRICELIST"]
						]
					]);

					$res = $getSettingsPriceList->fetch();
					
					if(!$res){
						
						$result = [
							"MAIN_PROPERTIES" 	=> [
								"NAME"			=> "Y",
								"PHOTOS"		=> "Y",
								"PRICE"			=> "Y",
								"SKLAD"			=> "Y",
								"DESCRIPTION"	=> "Y",
								"PROPERTIES"	=> "Y"
							],
							"OTHER_PROPERTIES" 	=> [
								"DONT_EXPORT_PLATFORM" 	=> $stores,
								"AVAILABLE"				=> [
									"NEW_PRODUCT"		=> "N",
									"UPDATE_PRODUCT"	=> "DELETE_EXPORT"
								],
								"PRICE" => [
									"PRICE_UPDATE"	=> 0,
									"PRICE_TYPE"	=> 1
								],
								"PRICES" => $prices,
								"DELIMETER" => ""
							],
						];

					}else{
						
						$result = json_decode($res["SETTINGS_PRICELIST"]);
						
					}
					
					echo json_encode([
						"RESULT" => [
							"RESULT" => $result,
							"ERRORS" => $errors
						]
					]);
					
				break;
				// Окончание получения настроек прайс-листа
				
				// Сохранение настроек прайс-листа
				case "savePropertiesSettings":
					
					$errors = [];
					
					$settingsList = [
						"MAIN_PROPERTIES" 	=> [
							"NAME"		=> ($data["PROPERTIES"]["MAIN_PROPERTIES"]["NAME"] == "true" || $data["PROPERTIES"]["MAIN_PROPERTIES"]["NAME"] == "Y" ? "Y" : "N"),
							"PHOTOS"	=> ($data["PROPERTIES"]["MAIN_PROPERTIES"]["PHOTOS"] == "true" || $data["PROPERTIES"]["MAIN_PROPERTIES"]["PHOTOS"] == "Y"  ? "Y" : "N"),
							"PRICE"	=> ($data["PROPERTIES"]["MAIN_PROPERTIES"]["PRICE"] == "true" || $data["PROPERTIES"]["MAIN_PROPERTIES"]["PRICE"] == "Y"  ? "Y" : "N"),
							"SKLAD"	=> ($data["PROPERTIES"]["MAIN_PROPERTIES"]["SKLAD"] == "true" || $data["PROPERTIES"]["MAIN_PROPERTIES"]["SKLAD"] == "Y"  ? "Y" : "N"),
							"DESCRIPTION"	=> ($data["PROPERTIES"]["MAIN_PROPERTIES"]["DESCRIPTION"] == "true" || $data["PROPERTIES"]["MAIN_PROPERTIES"]["DESCRIPTION"] == "Y"  ? "Y" : "N"),
							"PROPERTIES"	=> ($data["PROPERTIES"]["MAIN_PROPERTIES"]["PROPERTIES"] == "true" || $data["PROPERTIES"]["MAIN_PROPERTIES"]["PROPERTIES"] == "Y"  ? "Y" : "N")
						],
						"OTHER_PROPERTIES" 	=> [
							"DONT_EXPORT_PLATFORM" 	=> $data["PROPERTIES"]["OTHER_PROPERTIES"]["DONT_EXPORT_PLATFORM"],
							"AVAILABLE"				=> [
								"NEW_PRODUCT"		=> $data["PROPERTIES"]["OTHER_PROPERTIES"]["AVAILABLE"]["NEW_PRODUCT"],
								"UPDATE_PRODUCT"	=> $data["PROPERTIES"]["OTHER_PROPERTIES"]["AVAILABLE"]["UPDATE_PRODUCT"]
							],
							"PRICE" => [
								"PRICE_UPDATE"	=> $data["PROPERTIES"]["OTHER_PROPERTIES"]["PRICE"]["PRICE_UPDATE"],
								"PRICE_TYPE"	=> $data["PROPERTIES"]["OTHER_PROPERTIES"]["PRICE"]["PRICE_TYPE"]
							],
							"PRICES" => $data["PROPERTIES"]["OTHER_PROPERTIES"]["PRICES"],
							"DELIMETER" => $data["PROPERTIES"]["OTHER_PROPERTIES"]["DELIMETER"],
							"ADD_NAME" => $data["PROPERTIES"]["OTHER_PROPERTIES"]["ADD_NAME"]
						],
					];
					
					$getSettingsPriceList = \Xunit\Xmldropshipping\SettingsPriceListImportTable::getList([
						'select'  => [
							"ID",
							"SETTINGS_PRICELIST"
						],
						'filter'  => [
							"=ID_XML" => $data["ID_PRICELIST"]
						]
					]);

					$res = $getSettingsPriceList->fetch();
					
					if(!$res){
						
						$addSettingsPriceList = \Xunit\Xmldropshipping\SettingsPriceListImportTable::add([
							"ID_XML" 				=> $data["ID_PRICELIST"],
							"SETTINGS_PRICELIST" 	=> json_encode($settingsList)
						]);
						
						if($addSettingsPriceList->isSuccess()){
							
							$result = "Y";
							
							$mainReportAdd_ADD_SETTINGS_PRICE_LIST = \Xunit\Xmldropshipping\MainReportTable::add(
								[
									"ID_SELLER" 			=> $data["ID_SELLER"],
									"ID_XML" 				=> $data["ID_PRICELIST"],
									"STEP" 					=> 0,
									"TASK" 					=> "ADD_SETTINGS_PRICE_LIST",
									"STATUS" 				=> "SUCCESS",
									"COMMENTS_OR_ERRORS" 	=> "Настройки импорта успешно добавлены"
								]
							);
							
							/* Обновление таблицы STEPS */
							$getStepsThisPriceList = \Xunit\Xmldropshipping\StepsTable::getList(
								[
									"select" => [
										"ID",
										"ID_SELLER", 
										"ID_XML"
									],
									"filter" => [
										"ID_SELLER" => $data["ID_SELLER"],
										"ID_XML" 	=> $data["ID_PRICELIST"]
									]
								]
							);
							
							$getStepsThisPriceList = $getStepsThisPriceList->fetch();
							
							$mainStepsAdd_UPDATE_STEPS_SETTINGS = \Xunit\Xmldropshipping\StepsTable::update(
								$getStepsThisPriceList["ID"],
								[
									"ID_SELLER" 					=> $data["ID_SELLER"],
									"ID_XML" 						=> $data["ID_PRICELIST"],
									"ISSET_SETTINGS"				=> "Y"
								]
							);
							/* END Обновление таблицы STEPS */
							
						}else{
							
							$errors[] = "Ошибка при добавлении настроек импорта прайс-листа.";
							
						}
						
					}else{
						
						$updateSettingsPriceList = \Xunit\Xmldropshipping\SettingsPriceListImportTable::update($res["ID"], [
							"SETTINGS_PRICELIST" => json_encode($settingsList)
						]);
						
						if($updateSettingsPriceList->isSuccess()){
							
							$result = "Y";
							
							$mainReportAdd_UPDATE_SETTINGS_PRICE_LIST = \Xunit\Xmldropshipping\MainReportTable::add(
								[
									"ID_SELLER" 			=> $data["ID_SELLER"],
									"ID_XML" 				=> $data["ID_PRICELIST"],
									"STEP" 					=> 1,
									"TASK" 					=> "UPDATE_SETTINGS_PRICE_LIST",
									"STATUS" 				=> "SUCCESS",
									"COMMENTS_OR_ERRORS" 	=> "Настройки импорта успешно обновлены"
								]
							);
							
							/* Обновление таблицы STEPS */
							$getStepsThisPriceList = \Xunit\Xmldropshipping\StepsTable::getList(
								[
									"select" => [
										"ID",
										"ID_SELLER", 
										"ID_XML"
									],
									"filter" => [
										"ID_SELLER" => $data["ID_SELLER"],
										"ID_XML" 	=> $data["ID_PRICELIST"]
									]
								]
							);
							
							$getStepsThisPriceList = $getStepsThisPriceList->fetch();
							
							$mainStepsAdd_UPDATE_STEPS_SETTINGS = \Xunit\Xmldropshipping\StepsTable::update(
								$getStepsThisPriceList["ID"],
								[
									"ID_SELLER" 					=> $data["ID_SELLER"],
									"ID_XML" 						=> $data["ID_PRICELIST"],
									"ISSET_SETTINGS"				=> "Y"
								]
							);
							/* END Обновление таблицы STEPS */
							
						}else{
							
							$errors[] = "Ошибка при обновлении настроек импорта прайс-листа.";
							
						}
						
					}
					
					echo json_encode([
						"RESULT" => [
							"RESULT" => $result,
							"ERRORS" => $errors
						]
					]);
					
				break;
				// Окончание сохранения настроек прайс-листа
				
				case "GET_SPACE":
					
					function get_size($bytes){
				
						if($bytes < 1000 * 1024){
							
							return number_format($bytes / 1024, 2)." KB";
							
						}elseif($bytes < 1000 * 1048576){
							
							return number_format( $bytes / 1048576, 2 ) . " MB";
						
						}elseif($bytes < 1000 * 1073741824){
							
							return number_format($bytes / 1073741824, 2)." GB";
							
						}else{
							
							return number_format( $bytes / 1099511627776, 2 ) . " TB";
							
						}
						
					}
					
					$space = [
						"ALL_SPACE" => get_size(disk_total_space("/")),
						"FREE_SPACE" => get_size(disk_free_space("/"))
					];
					
					echo json_encode($space);
					
				break;
				
				// Установка в очередь на парсинг прайс-листа
				case "startImportPriceList":
					
					$errors = [];
					$result = [];
					
					$idPriceList = $data["ID_PRICELIST"];
					$idSeller = $data["ID_SELLER"];
					
					if(empty($idPriceList) || empty($idSeller)){
						
						$errors[] = "Критическая ошибка при старте парсинга прайс-листа.";
						
						$insertInfo = [
							"ID_PRICE_LIST" 		=> (empty($idPriceList) ? 0 : $idPriceList),
							"ID_SELLER" 			=> (empty($idSeller) ? 0 : $idSeller),
							"LAST_ID_IMPORT" 		=> 0,
							"DATE_TIME_IMPORT" 		=> date("Y-m-d H:i:s"),
							"TASK"					=> "START_IMPORT",
							"STEP" 					=> 0,
							"TEXT_IMPORT" 			=> implode(" ", $errors),
							"RESULT_STEP"		 	=> "N",
							"CANCEL_IMPORT"			=> "N"
						];
						
						$insertThisStep = \Xunit\Xmldropshipping\ReportParsingTable::add($insertInfo);
						
					}else{
						
						$getSettingsPriceList = \Xunit\Xmldropshipping\ReportParsingTable::getList([
							'select'  => [
								"ID_PRICE_LIST",
								"ID_SELLER",
								"LAST_ID_IMPORT"
							],
							'filter'  => [
								"ID_PRICE_LIST" => $idPriceList,
								"ID_SELLER" 	=> $idSeller
							],
							'order'	  => [
								"LAST_ID_IMPORT" => "DESC"
							],
							'limit'   => 1
						]);

						$resultGetSettingsPriceList = $getSettingsPriceList->fetch();
						
						$insertInfo = [
							"DATE_TIME_IMPORT" 		=> date("Y-m-d H:i:s"),
							"TASK"					=> "START_IMPORT",
							"STEP" 					=> 0,
							"TEXT_IMPORT" 			=> "Успешная установка в очередь прайс-листа",
							"RESULT_STEP"		 	=> "Y",
							"CANCEL_IMPORT"			=> "N"
						];
						
						if(empty($resultGetSettingsPriceList)){
							
							$insertInfo["ID_PRICE_LIST"] = $idPriceList;
							$insertInfo["ID_SELLER"] = $idSeller;
							$insertInfo["LAST_ID_IMPORT"] = 1;
							
						}else{
							
							$insertInfo["ID_PRICE_LIST"] = $resultGetSettingsPriceList["ID_PRICE_LIST"];
							$insertInfo["ID_SELLER"] = $resultGetSettingsPriceList["ID_SELLER"];
							$insertInfo["LAST_ID_IMPORT"] = $resultGetSettingsPriceList["LAST_ID_IMPORT"] + 1;
							
						}
						
						$insertThisStep = \Xunit\Xmldropshipping\ReportParsingTable::add($insertInfo);

						$params = [];
						
						$params["STEP_IMPORT"] = $insertInfo["STEP"];
						$params["ID_PRICELIST"] = $insertInfo["ID_PRICE_LIST"];
						$params["ID_SELLER"] = $insertInfo["ID_SELLER"];
						$params["LAST_ID_IMPORT"] = $insertInfo["LAST_ID_IMPORT"];
						
						/*
						$stepImport
						$priceListId
						$priceListSellerId
						$lastIdImport
						$idListProduct
						$offsetInImport
						$data
						*/
						
						$rab = new rabbit();
						$rab->rabbitNewConnection();
						$rab->rabbitNewChanel();
						$rab->rabbitNewExchange();
						$rab->rabbitSetExchange("parsing", AMQP_EX_TYPE_DIRECT, AMQP_DURABLE);
						$rab->rabbitNewQueue();
							
						$rab->rabbitSetQueue("parsing", AMQP_IFUNUSED | AMQP_AUTODELETE, "parsing");
						$rab->rabbitSendToExchange($params, "parsing");
						
						$rab->rabbitCloseChanel();
						$rab->rabbitDisconnect();
							
						$result = "Y";
						
					}
					
					echo json_encode([
						"RESULT" 			=> ($result == "Y" ? $result : "N"),
						"LAST_ID_IMPORT" 	=> $insertInfo["LAST_ID_IMPORT"],
						"ERRORS" 			=> $errors
					]);
					
				break;
				// Окончание установки в очередь на парсинг прайс-листа
				
				// Получение результата импорта
				case "getResultImport":
					
					$result = [];
					$errors = [];
					
					$idPriceList = $data["ID_PRICELIST"];
					$lastIdImport = $data["LAST_ID_IMPORT"];
					
					$getInfoReportPriceList = \Xunit\Xmldropshipping\ReportParsingTable::getList([
						'filter' => [
							"=ID_PRICE_LIST" 	=> $idPriceList,
							"=LAST_ID_IMPORT" 	=> $lastIdImport
						]
					]);

					$result = $getInfoReportPriceList->fetchAll();
					
					echo json_encode([
						"RESULT" => [
							"RESULT" => $result,
							"ERRORS" => $errors
						]
					]);
					
				break;
				// Окончание получения результата импорта
				
				/*
				case "getResultCancelImport":
					
					$result = [];
					$errors = [];
					
					$idPriceList = $data["ID_PRICELIST"];
					$lastIdImport = $data["LAST_ID_IMPORT"];
					
					$getInfoReportPriceList = \Xunit\Xmldropshipping\ReportParsingTable::getList([
						'filter' => [
							"=ID_PRICE_LIST" 	=> $idPriceList,
							"=LAST_ID_IMPORT" 	=> $lastIdImport,
							"CANCEL_IMPORT"		=> "Y"
						]
					]);

					$result = $getInfoReportPriceList->fetchAll();
					
					echo json_encode([
						"RESULT" => [
							"RESULT" => $result,
							"ERRORS" => $errors
						]
					]);
					
				break;
				*/
				
				case "getSettingsTimeXml":
					
					$result = [];
					$errors = [];
					
					$idPriceList = $data["ID_PRICELIST"];
					$idSeller = $data["ID_SELLER"];
					
					$getTimeTableSettings = \Xunit\Xmldropshipping\AutoUpdatePriceListTable::getList(
						[
							"select" => [
								"ACTIVE",
								"TIME_SETTING"
							],
							"filter" => [
								"ID_SELLER" => $idSeller,
								"ID_XML" 	=> $idPriceList
							]
						]
					);
					
					$getTimeTableSettings = $getTimeTableSettings->fetch();
					
					if(empty($getTimeTableSettings)){
						
						$result = [
							"ACTIVE" 		=> "N",
							"TIME_SETTING" 	=> "",
						];
						
					}else{
						
						$result = $getTimeTableSettings;
						
					}
					
					echo json_encode([
						"RESULT" => $result,
						"ERRORS" => $errors
					]);
					
				break;
				
				case "savePropertiesTimeSettings":
					
					$errors = [];
					
					if(empty($data["TIME_SETTING"])){
						
						$errors[] = "Не выбран промежуток времени.";
						
					}
					
					if(empty($errors)){
					
						$getSettingsPriceList = \Xunit\Xmldropshipping\AutoUpdatePriceListTable::getList([
							'select'  => [
								"ID",
								"ID_AGENT"
							],
							'filter'  => [
								"ID_XML" 	=> $data["ID_PRICELIST"],
								"ID_SELLER" => $data["ID_SELLER"]
							]
						]);

						$res = $getSettingsPriceList->fetch();
						
						if(!$res){
							
							if($data["ACTIVE"] == "true"){
							
								$dateNow = date("d.m.Y H:i:s");
								$dateNowPlusIntrval = date("d.m.Y H:i:s", strtotime(date("d.m.Y H:i:s")." +1 minutes"));
								
								$agentId = CAgent::AddAgent(
									"Cron::worker(".$data["ID_SELLER"].", ".$data["ID_PRICELIST"].");",
									"xunit.xmldropshipping",
									"N",
									(!empty($data["TIME_SETTING"]) ? $data["TIME_SETTING"] : 3600),
									$dateNow,
									"Y",
									$dateNowPlusIntrval
								);
							
							}
							
							$addSettingsPriceList = \Xunit\Xmldropshipping\AutoUpdatePriceListTable::add([
								"ID_SELLER" 	=> $data["ID_SELLER"],
								"ID_XML" 		=> $data["ID_PRICELIST"],
								"ACTIVE" 		=> ($data["ACTIVE"] == "true" ? "Y" : "N"),
								"TIME_SETTING" 	=> (!empty($data["TIME_SETTING"]) ? $data["TIME_SETTING"] : 3600),
								"ID_AGENT"		=> $agentId
							]);
							
							if($addSettingsPriceList->isSuccess()){
								
								$result = "Y";
								
							}else{
								
								$errors[] = "Ошибка при добавлении настроек импорта прайс-листа.";
								
							}
							
						}else{
							
							CAgent::Delete($res["ID_AGENT"]);
							
							if($data["ACTIVE"] == "true"){
							
								$dateNow = date("d.m.Y H:i:s");
								$dateNowPlusIntrval = date("d.m.Y H:i:s", strtotime(date("d.m.Y H:i:s")." +1 minutes"));
								
								$agentId = CAgent::AddAgent(
									"Cron::worker(".$data["ID_SELLER"].", ".$data["ID_PRICELIST"].");",
									"xunit.xmldropshipping",
									"N",
									(!empty($data["TIME_SETTING"]) ? $data["TIME_SETTING"] : 3600),
									$dateNow,
									"Y",
									$dateNowPlusIntrval
								);
							
							}
							
							$updateSettingsPriceList = \Xunit\Xmldropshipping\AutoUpdatePriceListTable::update($res["ID"], [
								"ID_SELLER" 	=> $data["ID_SELLER"],
								"ID_XML" 		=> $data["ID_PRICELIST"],
								"ACTIVE" 		=> ($data["ACTIVE"] == "true" ? "Y" : "N"),
								"TIME_SETTING" 	=> (!empty($data["TIME_SETTING"]) ? $data["TIME_SETTING"] : 3600),
								"ID_AGENT"		=> $agentId
							]);
							
							if($updateSettingsPriceList->isSuccess()){
								
								$result = "Y";
								
							}else{
								
								$errors[] = "Ошибка при обновлении настроек импорта прайс-листа.";
								
							}
							
						}
						
					}
					
					echo json_encode([
						"RESULT" => [
							"RESULT" => $result,
							"ERRORS" => $errors
						]
					]);
					
				break;
				
				case "cancelImportPriceList":
				
					$result = [];
					$errors = [];
					
					$idPriceList = $data["ID_PRICELIST"];
					$idSeller = $data["ID_SELLER"];
					
					$getLastIdImport = \Xunit\Xmldropshipping\ReportParsingTable::getList([
						'select'  => [
							"ID_PRICE_LIST",
							"LAST_ID_IMPORT"
						],
						'filter'  => [
							"ID_PRICE_LIST" => $idPriceList
						],
						'order'	  => [
							"LAST_ID_IMPORT" => "DESC"
						],
						'limit'   => 1
					]);

					$resultGetLastIdImport = $getLastIdImport->fetch();
					/*
					$insertInfo = [
						"ID_PRICE_LIST" 		=> $idPriceList,
						"ID_SELLER" 			=> $idSeller,
						"LAST_ID_IMPORT" 		=> $resultGetLastIdImport["LAST_ID_IMPORT"],
						"DATE_TIME_IMPORT" 		=> date("Y-m-d H:i:s"),
						"TASK"					=> "CANCEL_IMPORT",
						"STEP" 					=> "0",
						"TEXT_IMPORT" 			=> "Идёт принудительная остановка импорта",
						"RESULT_STEP"		 	=> "Y",
						"CANCEL_IMPORT"			=> "Y"
					];
					*/
					
					$insertInfo = [
						"ID_PRICE_LIST" 		=> $idPriceList,
						"ID_SELLER" 			=> $idSeller,
						"LAST_ID_IMPORT" 		=> $resultGetLastIdImport["LAST_ID_IMPORT"],
						"DATE_TIME_IMPORT" 		=> date("Y-m-d H:i:s"),
						"TASK"					=> "CANCEL_IMPORT",
						"STEP" 					=> 0,
						"TEXT_IMPORT" 			=> "Отмена импорта",
						"RESULT_STEP"		 	=> "Y",
						"CANCEL_IMPORT"			=> "Y"
					];
						
					$insertThisStep = \Xunit\Xmldropshipping\ReportParsingTable::add($insertInfo);
					
					$result = "Y";
					
					/*
					if($insertThisStep->isSuccess()){
						
						$params["STEP_IMPORT"] = "CANCEL_IMPORT";
						$params["ID_PRICELIST"] = $idPriceList;
						$params["ID_SELLER"] = $idSeller;
						$params["LAST_ID_IMPORT"] = $resultGetLastIdImport["LAST_ID_IMPORT"];
						
						//$stepImport
						//$priceListId
						//$priceListSellerId
						//$lastIdImport
						//$idListProduct
						//$offsetInImport
						//$data
						
						$rab = new rabbit();
						$rab->rabbitNewConnection();
						$rab->rabbitNewChanel();
						$rab->rabbitNewExchange();
						$rab->rabbitSetExchange("parsing", AMQP_EX_TYPE_DIRECT, AMQP_DURABLE);
						$rab->rabbitNewQueue();
							
						$rab->rabbitSetQueue("parsing", AMQP_IFUNUSED | AMQP_AUTODELETE, "parsing");
						$rab->rabbitSendToExchange($params, "parsing");
						
						$rab->rabbitCloseChanel();
						$rab->rabbitDisconnect();
						
						$result = "Y";
						
					}
					*/
					
					echo json_encode([
						"RESULT" => [
							"RESULT" 			=> $result,
							"LAST_ID_IMPORT"	=> $resultGetLastIdImport["LAST_ID_IMPORT"]
						],
						"ERRORS" => $errors
					]);
				
				break;
				
				case "deleteProductThisPriceList":
					
					$result = "";
					$errors = [];
					
					$idSeller = $data["ID_SELLER"];
					$idPriceList = $data["ID_SELLER_XML"];
					
					if(!empty($idSeller) && !empty($idPriceList)){
					
						$iblockList = [];

						$res = CIBlock::GetList(
							[], 
							[
								'TYPE' => 'offers'
							]
						);
									
						while($ar_res = $res->Fetch()){

							$iblockList[$ar_res["ID"]] = $ar_res["ID"];

						}
						
						$getLastIdImport = \Xunit\Xmldropshipping\ReportParsingTable::getList([
							'select'  => [
								"ID_PRICE_LIST",
								"LAST_ID_IMPORT"
							],
							'filter'  => [
								"=ID_PRICE_LIST" => $idPriceList
							],
							'order'	  => [
								"LAST_ID_IMPORT" => "DESC"
							],
							'limit'   => 1
						]);

						$res = $getLastIdImport->fetch();
						
						$lastIdImport = $res["LAST_ID_IMPORT"] + 1;
						
						$params["STEP_IMPORT"] = 7;
						$params["ID_PRICE_LIST"] = $idPriceList;
						$params["ID_SELLER"] = $idSeller;
						$params["LAST_ID_IMPORT"] = $lastIdImport;
						$params["STEP_IMPORT_INNER"] = "DELETE_ALL_PRODUCT_THIS_PRICE_LIST";
						$params["IBLOCK_LIST"] = $iblockList;
						$params["RECORD_TO_LOG"] = "Y";
						
						/*
						$stepImport
						$priceListId
						$priceListSellerId
						$lastIdImport
						$idListProduct
						$offsetInImport
						$data
						*/
						
						$rab = new rabbit();
						$rab->rabbitNewConnection();
						$rab->rabbitNewChanel();						
						$rab->rabbitNewExchange();
						$rab->rabbitSetExchange("parsing", AMQP_EX_TYPE_DIRECT, AMQP_DURABLE);
						$rab->rabbitNewQueue();

						$rab->rabbitSetQueue("parsing", AMQP_IFUNUSED | AMQP_AUTODELETE, "parsing");
						$rab->rabbitSendToExchange($params, "parsing");
						
						$rab->rabbitCloseChanel();
						$rab->rabbitDisconnect();
					
						$insertInfo = [
							"ID_PRICE_LIST" 		=> $idPriceList,
							"DATE_TIME_IMPORT" 		=> date("Y-m-d H:i:s"),
							"STATUS_IMPORT" 		=> "17",
							"TEXT_IMPORT" 			=> "Приступили к удалению всех товаров по данному прайс-листу",
							"RESULT_STATUS_IMPORT" 	=> "Y",
							"LAST_ID_IMPORT" 		=> $lastIdImport,
							"CANCEL_IMPORT"			=> "N"
						];
							
						$insertThisStep = \Xunit\Xmldropshipping\ReportParsingTable::add($insertInfo);
						
						if($insertThisStep->isSuccess()){
					
							$result = "Y";
					
						}
					
					}else{
						
						$errors[] = "Отсутствует информация по поставщику либо по его прайс-листу.";
						
					}
					
					echo json_encode([
						"RESULT" => $result,
						"ERRORS" => $errors
					]);
					
				break;
				
				case "getResultDeleteAllProduct":
					
					$result = [];
					$errors = [];
					
					$idPriceList = $data["ID_PRICE_LIST"];

					$getLastIdImport = \Xunit\Xmldropshipping\ReportParsingTable::getList([
						'select'  => [
							"ID_PRICE_LIST",
							"LAST_ID_IMPORT"
						],
						'filter'  => [
							"=ID_PRICE_LIST" => $idPriceList
						],
						'order'	  => [
							"LAST_ID_IMPORT" => "DESC"
						],
						'limit'   => 1
					]);

					$res = $getLastIdImport->fetch();

					$getResultDeleteAllProduct = \Xunit\Xmldropshipping\ReportParsingTable::getList([
						'select'  => ["*"],
						'filter'  => [
							"=ID_PRICE_LIST" 	=> $idPriceList,
							"STATUS_IMPORT" 	=> [17, 18],
							"LAST_ID_IMPORT" 	=> $res["LAST_ID_IMPORT"]
						],
						'order'	  => [
							"ID" => "DESC"
						],
						'limit'	=> 2
					]);
					
					$getResultDeleteAllProduct = $getResultDeleteAllProduct->fetchAll();
					
					echo json_encode([
						"RESULT" => $getResultDeleteAllProduct,
						"ERRORS" => $errors
					]);
					
				break;
				
			}
		
		die();
		
	}
	
	$APPLICATION->SetTitle("Добавить поставщика");
	
	// Подключение библиотек
	Main\Page\Asset::getInstance()->addJs('/bitrix/js/main/file_dialog.js');
	
	// Подключаем языковые константы
	Loc::LoadMessages(__FILE__);

	// Получаем все настройки модуля
	$optionsModule = \Bitrix\Main\Config\Option::getForModule($module_id);
	
	// Описание вкладок
	if($ID > 0){
	
		$aTabs = [
			[
				"DIV" 	=> "edit1",
				"TAB" 	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_EDIT_TAB_1"),
				'TITLE'	=> Loc::getMessage('XUNIT_XMLDROPSHIPPING_EDIT_TAB_1_1')
			],
			[
				"DIV" 	=> "edit2",
				"TAB" 	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_EDIT_TAB_2"),
				'TITLE'	=> Loc::getMessage('XUNIT_XMLDROPSHIPPING_EDIT_TAB_2_1')
			],
			[
				"DIV" 	=> "edit3",
				"TAB" 	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_EDIT_TAB_3"),
				'TITLE'	=> Loc::getMessage('XUNIT_XMLDROPSHIPPING_EDIT_TAB_3_1')
			],
			[
				"DIV" 	=> "edit4",
				"TAB" 	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_EDIT_TAB_4")
			],
		];
		
		
	
	}else{
		
		$aTabs = [
			[
				"DIV" 	=> "edit1",
				"TAB" 	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_EDIT_TAB_1"),
				'TITLE'	=> Loc::getMessage('XUNIT_XMLDROPSHIPPING_EDIT_TAB_1_1')
			]
		];
		
	}
	
	$tabControl = new CAdminTabControl("tabControl", $aTabs);
	
	// Контекстное меню
	if($ID > 0){
		
		$aMenu[] = array(
			"TEXT"	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_LIST_SELLER"),
			"LINK"	=> "sellers.php?lang=".LANG,
			"ICON"	=> "btn_list",
		);
		
		$aMenu[] = array(
			"TEXT"	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_ADD_SELLER"),
			"LINK"	=> "sellers_edit.php?lang=".LANG,
			"ICON"	=> "btn_new",
		);
		
		$aMenu[] = array(
			"TEXT"	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_DELETE"),
			"LINK"	=>"javascript:if(confirm('".GetMessage("rubric_mnu_del_conf")."'))window.location='sellers.php?ID=".$ID."&action=delete&lang=".LANG."&".bitrix_sessid_get()."';",
			"ICON" 	=> "btn_delete",
		);
	  
	}else{
		
		$aMenu[] = array(
			"TEXT"	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_LIST_SELLER"),
			"LINK"	=> "sellers.php?lang=".LANG,
			"ICON"	=> "btn_list",
		);
		
	}
	
	if($ID > 0 && $sellerInfo = \Xunit\Xmldropshipping\SellersTable::getById($ID)){
		
		$sellerInfo = $sellerInfo->fetch();
		
		$sellerXsdGetInfo = \Xunit\Xmldropshipping\XsdTable::getList([
			'select'  => ["*"],
			'filter'  => [
				"=ID_SELLER" => $ID
			]
		]);
		
		$sellerXsdInfo = $sellerXsdGetInfo->fetchAll();
		
		$sellerXsdInfoTemp = [];
		
		foreach($sellerXsdInfo as $item){
			
			$sellerXsdInfoTemp[$item["ID"]] = $item;
			
		}
		
		$sellerXsdInfo = $sellerXsdInfoTemp;
		
		unset($sellerXsdInfoTemp);
		
		$sellerXmlGetInfo = \Xunit\Xmldropshipping\XmlTable::getList([
			'select'  => ["*"],
			'filter'  => [
				"=ID_SELLER" => $ID
			]
		]);
		
		$sellerXmlInfo = $sellerXmlGetInfo->fetchAll();
		
		foreach($sellerXmlInfo as $key => $val){
			
			$getStepsThisPriceList = \Xunit\Xmldropshipping\StepsTable::getList(
				[
					"select" => [
						"ID",
						"ID_SELLER", 
						"ID_XML",
						"ISSET_SETTINGS",
						"ANALIZE_PRICE_LIST"
					],
					"filter" => [
						"ID_SELLER" => $ID,
						"ID_XML" 	=> $val["ID"]
					]
				]
			);
						
			$getStepsThisPriceList = $getStepsThisPriceList->fetch();
			
			/*
			$getReportParsing = \Xunit\Xmldropshipping\ReportParsingTable::getList([
				'select'	=> ["*"],
				'filter'	=> [
					"=ID_PRICE_LIST" => $val["ID"]
				],
				'order'		=> [
					"LAST_ID_IMPORT" 	=> "DESC",
					"ID"				=> "ASC"
				]
			]);
			
			$resultGetReportParsing = $getReportParsing->fetchAll();
			
			$lastIdImport = $resultGetReportParsing[0]["LAST_ID_IMPORT"];
			
			$resultSteps = [];
			
			foreach($resultGetReportParsing as $item){
				
				if($item["LAST_ID_IMPORT"] == $lastIdImport){
					
					$resultSteps[] = $item;
					
				}
				
			}
			*/
			
			
			$sellerXmlInfo[$key]["STEPS"] = $getStepsThisPriceList;
			
			$getTimeTableSettings = \Xunit\Xmldropshipping\AutoUpdatePriceListTable::getList(
				[
					"select" => [
						"ID",
						"ACTIVE",
						"TIME_SETTING",
						"ID_AGENT"
					],
					"filter" => [
						"ID_SELLER" => $ID,
						"ID_XML" 	=> $val["ID"]
					]
				]
			);
			
			$getTimeTableSettings = $getTimeTableSettings->fetch();
			
			if($getTimeTableSettings["ACTIVE"] == "Y"){
				
				$sellerXmlInfo[$key]["STEPS"]["AUTOUPDATE"] = "Y";
				
			}else{
				
				$sellerXmlInfo[$key]["STEPS"]["AUTOUPDATE"] = "N";
				
			}
			
		}
		
	}
	
?>

<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';?>

<?
	// Контекстное меню
	$context = new CAdminContextMenu($aMenu);
	$context->Show();
?>

	<div class="space-absolute"></div>
	
	<script>
	
		function getSpace(){

			BX.ajax({
				url: '<?=$APPLICATION->GetCurPage(false);?>',
				data: {
					'type': 'GET_SPACE'
				},
				method: 'POST',
				dataType: 'json',
				timeout: 30,
				async: true,
				processData: true,
				scriptsRunFirst: true,
				emulateOnload: true,
				start: true,
				cache: false,
				onsuccess: function(data){
					
					var result = "Всего памяти: <b>" + data["ALL_SPACE"] + "</b>. Свободно памяти: <b>" + data["FREE_SPACE"] + "</b>."
					
					document.querySelector(".space-absolute").innerHTML = result;
					
				},
				onfailure: function(error){
					
					console.log(error);

				}
			}); 

		}
		
		getSpace();
		
		setInterval(getSpace, 30000);

	</script>				
	
	<!-- Стартуем вкладки -->
	<?$tabControl->Begin();?>

		<!-- Вкладка "Информация" -->
		<?$tabControl->BeginNextTab();?>

			<input type="hidden" name="ID_SELLER" value="<?=$ID;?>">

			<tr>
				<td><?=Loc::getMessage("XUNIT_XMLDROPSHIPPING_FORM_NAME_SELLER");?></td>
				<td><input type="text" name="NAME_SELLER" value="<?=$sellerInfo["NAME_SELLER"];?>" size="30" maxlength="200"></td>
			</tr>
			
			<tr>
				<td><?=Loc::getMessage("XUNIT_XMLDROPSHIPPING_FORM_FIO_SELLER");?></td>
				<td><input type="text" name="FIO_SELLER" value="<?=$sellerInfo["FIO_SELLER"];?>" size="30" maxlength="200"></td>
			</tr>
			
			<tr>
				<td><?=Loc::getMessage("XUNIT_XMLDROPSHIPPING_FORM_SITE_SELLER");?></td>
				<td><input type="text" name="SITE_SELLER" value="<?=$sellerInfo["SITE_SELLER"];?>" size="30" maxlength="200"></td>
			</tr>
			
			<tr>
				<td><?=Loc::getMessage("XUNIT_XMLDROPSHIPPING_FORM_TELEPHONE_SELLER");?></td>
				<td><input type="text" name="TELEPHONE_SELLER" value="<?=$sellerInfo["TELEPHONE_SELLER"];?>" size="30" maxlength="200"></td>
			</tr>
			
			<tr>
				<td valign="top"><?=Loc::getMessage("XUNIT_XMLDROPSHIPPING_FORM_DESCRIPTION_SELLER");?></td>
				<td><textarea class="typearea" name="DESCRIPTION_SELLER" style="width:100%" rows="10" wrap="VIRTUAL"><?=$sellerInfo["DESCRIPTION_SELLER"];?></textarea></td>
			</tr>
			
			<tr>
				<td></td>
				<td><input type="button" name="saveInfoSeller" value="Сохранить" onclick="saveInfoSeller();"></td>
			</tr>
			
			<tr>
				<td></td>
				<td class="errors-tab-1"><ul></ul></td>
			</tr>
			
			<script>
				
				function saveInfoSeller(){
					
					var data = [];
						data["ID_SELLER"] = document.querySelector("input[name='ID_SELLER']").value;
						data["NAME_SELLER"] = document.querySelector("input[name='NAME_SELLER']").value;
						data["FIO_SELLER"] = document.querySelector("input[name='FIO_SELLER']").value;
						data["SITE_SELLER"] = document.querySelector("input[name='SITE_SELLER']").value;
						data["TELEPHONE_SELLER"] = document.querySelector("input[name='TELEPHONE_SELLER']").value;
						data["DESCRIPTION_SELLER"] = document.querySelector("textarea[name='DESCRIPTION_SELLER']").value;
					
					BX.ajax({
						url: '<?=$APPLICATION->GetCurPage(false);?>',
						data: {
							'type': 'saveInfoSeller',
							'data': data
						},
						method: 'POST',
						dataType: 'json',
						timeout: 30,
						async: true,
						processData: true,
						scriptsRunFirst: true,
						emulateOnload: true,
						start: true,
						cache: false,
						onsuccess: function(data){
							
							document.querySelector("#edit1_edit_table .errors-tab-1 ul").innerHTML = "";
							
							if(data["RESULT"]["RESULT"]["SUCCESS"] == "Y" && data["RESULT"]["ERRORS"].length == 0){
								
								window.location.href = data["RESULT"]["RESULT"]["URL_REDIRECT"];
								
							}
							
							if(data["RESULT"]["ERRORS"].length > 0){
								
								var errorsHtml = "";
								
								for(error in data["RESULT"]["ERRORS"]){
									
									errorsHtml += "<li>" + data["RESULT"]["ERRORS"][error] + "</li>";
									
								}
								
								document.querySelector("#edit1_edit_table .errors-tab-1 ul").innerHTML = errorsHtml;
								
							}
							
						},
						onfailure: function(error){

							console.log(error);

						}
					}); 
					
				}
				
			</script>
			<!-- Окончание вкладки "Информация" -->
 
		<?if($ID > 0){?>
 
			<!-- Вкладка "Прайслисты на нашем сервере" -->
			<?$tabControl->BeginNextTab();?>
				
				<tr>
				
					<td>
						
						<?=BeginNote();?>
					
							Для того, чтобы прайс-лист поставщика проверить на синтаксические ошибки - нужно составить XSD-схему самого прайс-листа от поставщика. Для этого следует перейти по <a href="https://www.freeformatter.com/xsd-generator.html" target="_blank">ссылке</a> и скопипастить весь прайс-лист в поле "Option 1: Copy-paste your XML document here". Затем нажать на кнопку "Generate XSD". Далее загружаем сгенерированный файл ниже.
							
						<?=EndNote();?>
						
						<table class="xsd-table">
							
							<?if(!empty($sellerXsdInfo)){?>
							
							<?$i = 1;?>
							
							<tr class="xsd-table-tr-main">
									
								<td>
									
									<table class="xsd-table-tr-main-table">
										
										<tr>

											<td>#</td>
											<td>Название XSD схемы</td>
											<td>Путь к XSD схеме</td>
											<td>Действия</td>

										</tr>

										<?foreach($sellerXsdInfo as $xsd){?>
								
											<tr>

												<td><?=$i;?></td>
												<td><?=$xsd["FILE_XSD_NAME_SELLER"];?></td>
												<td><?=$xsd["FILE_XSD_LINK_SELLER"];?></td>
												<td><input type="button" name="deleteXsd" value="Удалить XSD схему" OnClick="deleteXsd(<?=$xsd["ID"];?>);"></td>

											</tr>
								
										<?$i++;?>
								
										<?}?>
							
									</table>
										
								</td>
									
							</tr>
							
							<tr>
								
								<td>
							
									<br/><br/>
									
								</td>

							</tr>
							
							<?}?>
							
							<tr>
								
								<td>
									
									Добавить XSD схему
									
								</td>
								
							</tr>
							
							<tr class="xsd-table-tr">
								
								<td>
								
									<span class='count-row'>#1</span>
								
									<input type="text" name="FILE_NEW_XSD_NAME" size="30" maxlength="500" value="" placeholder="Наименование xsd-схемы">
									<input type="text" name="FILE_NEW_XSD_FILE" size="30" maxlength="500" value="" placeholder="Путь к файлу на сервере">
									<input type="button" name="browse" value="Выбрать / Загрузить файл" OnClick="browseFile(this, 'xsd');">
									<input type="button" name="deleteRow" value="Удалить строку" OnClick="deleteRow(this);">
								
								</td>
								
							</tr>
							
							<tr class="xsd-table-tr-new">
								
								<td>
								
									<input type="button" name="browse" value="Добавить строку" OnClick="addNewRowXsd();">
								
								</td>
								
							</tr>
							
							<tr>
								
								<td>
								
									<br/>
									
									<input type="button" name="saveXsdSeller" value="Сохранить" onclick="saveXsdSeller();">
									
									<br/><br/>
									
									<div class="errors-tab-2"><ul></ul></div>
									
								</td>
								
							</tr>
							
						</table>
						
						<script>
							
							var timerToImport = [];
							
							function deleteXsd(id){
								
								var deleteXsd = confirm("Внимание! При удалении XSD схемы удалится прайс-лист, к которому прикреплена данная XSD схема . \r Точно удаляем данную XSD схему ?");
									
									if(deleteXsd){
										
										var data = [];
											data["ID_SELLER"] = <?=$ID;?>;
											data["ID_SELLER_XSD"] = id;
										
										BX.ajax({
											url: '<?=$APPLICATION->GetCurPage(false);?>',
											data: {
												'type': 'deleteXsdSeller',
												'data': data
											},
											method: 'POST',
											dataType: 'json',
											timeout: 30,
											async: true,
											processData: true,
											scriptsRunFirst: true,
											emulateOnload: true,
											start: true,
											cache: false,
											onsuccess: function(data){
												
												if(data["RESULT"]["ERRORS"].length == 0 && data["RESULT"]["RESULT"] == "Y"){
											
													window.location.href = "<?=$APPLICATION->GetCurUri();?>";
													
												}
												
											},
											onfailure: function(error){

												console.log(error);

											}
										});
										
									}
								
							}
							/*
							var timerDeleteAllProduct = [];
							
							function deleteProductThisPriceList(id){
								
								var idPriceList = id;
								
								var thisDeleteButtonInTableXmlPrice = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteXml']");
								
								var matchPropertyButton = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='matchPropertyButton']");
								
								var importSettings = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importSettings']");
								
								var importPriceList = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importPriceList']");
								
								var deleteProductThisPriceList = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteProductThisPriceList']");
								
								thisDeleteButtonInTableXmlPrice.disabled = true;
								matchPropertyButton.disabled = true;
								importSettings.disabled = true;
								importPriceList.disabled = true;
								deleteProductThisPriceList.disabled = true;
								
								var delete_1 = confirm("Данное действие удалит все товары, которые были загружены из этого прайс-листа когда-либо. Продолжить ?");
								
								if(delete_1){
								
									var delete_2 = confirm("Точно ?");
									
									if(delete_2){
										
										var data = [];
											data["ID_SELLER"] = <?=$ID;?>;
											data["ID_SELLER_XML"] = id;
										
										BX.ajax({
											url: '<?=$APPLICATION->GetCurPage(false);?>',
											data: {
												'type': 'deleteProductThisPriceList',
												'data': data
											},
											method: 'POST',
											dataType: 'json',
											timeout: 30,
											async: true,
											processData: true,
											scriptsRunFirst: true,
											emulateOnload: true,
											start: true,
											cache: false,
											onsuccess: function(data){
												
												document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " .note-controls .errors-tab-3-1 ul").innerHTML = "";
												
												if(data["ERRORS"].length == 0 && data["RESULT"] == "Y"){
											
													document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " .note-controls").style = "visibility: visible;";
													
													document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " .note-controls .errors-tab-3-1 ul").innerHTML = "<li>Приступили к удалению всех товаров по данному прайс-листу</li>";
													
													timerDeleteAllProduct[idPriceList] = setInterval(getResultDeleteAllProduct, 3000, idPriceList);
											
												}
												
											},
											onfailure: function(error){

												console.log(error);

											}
										});
										
									}
									
								}
								
							}
							
							function getResultDeleteAllProduct(idPriceList){
								
								var thisDeleteButtonInTableXmlPrice = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteXml']");
								
								var matchPropertyButton = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='matchPropertyButton']");
								
								var importSettings = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importSettings']");
								
								var importPriceList = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importPriceList']");
								
								var deleteProductThisPriceList = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteProductThisPriceList']");
								
								var data = [];
									data["ID_PRICE_LIST"] = idPriceList;
										
									BX.ajax({
										url: '<?=$APPLICATION->GetCurPage(false);?>',
										data: {
											'type': 'getResultDeleteAllProduct',
											'data': data
										},
										method: 'POST',
										dataType: 'json',
										timeout: 30,
										async: true,
										processData: true,
										scriptsRunFirst: true,
										emulateOnload: true,
										start: true,
										cache: false,
										onsuccess: function(data){
											
											document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " .note-controls .errors-tab-3-1 ul").innerHTML = "";
											
											document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " .note-controls").style = "visibility: visible;";
											
											if(data["ERRORS"].length == 0 && data["RESULT"].length > 0){
												
												var result = "";
												
												for(item in data["RESULT"]){
													
													result += "<li>" + data["RESULT"][item]["TEXT_IMPORT"] + "</li>";
													
													if(data["RESULT"][item]["STATUS_IMPORT"] == "18"){
														
														clearInterval(timerDeleteAllProduct[idPriceList]);
														
														thisDeleteButtonInTableXmlPrice.disabled = false;
														matchPropertyButton.disabled = false;
														importSettings.disabled = false;
														importPriceList.disabled = false;
														deleteProductThisPriceList.disabled = false;
														
													}
													
												}
												
											}
											
											document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " .note-controls .errors-tab-3-1 ul").innerHTML = result;
												
										},
										onfailure: function(error){

											console.log(error);

										}
									});
								
							}
							*/
							
							function deleteXml(id){
								
								var deleteXml = confirm("Действительно удалить данный прайс-лист ?");
									
									if(deleteXml){
										
										var data = [];
											data["ID_SELLER"] = <?=$ID;?>;
											data["ID_SELLER_XML"] = id;
										
										BX.ajax({
											url: '<?=$APPLICATION->GetCurPage(false);?>',
											data: {
												'type': 'deleteXmlSeller',
												'data': data
											},
											method: 'POST',
											dataType: 'json',
											timeout: 30,
											async: true,
											processData: true,
											scriptsRunFirst: true,
											emulateOnload: true,
											start: true,
											cache: false,
											onsuccess: function(data){
												
												console.log(data);
												
												if(data["RESULT"]["ERRORS"].length == 0 && data["RESULT"]["RESULT"] == "Y"){
											
													window.location.href = "<?=$APPLICATION->GetCurUri();?>";
													
												}
												
											},
											onfailure: function(error){

												console.log(error);

											}
										});
										
									}
								
							}
							
							function deleteRowXml(row){
								
								row.parentNode.parentNode.remove();
								
								var beforeRowLength = document.querySelectorAll("#edit3_edit_table .xml-table .xml-table-tr");
								
									for(var i = 0; i < beforeRowLength.length; i++){
										
										var number = i + 1;
										
										document.querySelectorAll("#edit3_edit_table .xml-table .xml-table-tr")[i].querySelector(".count-row").innerHTML = "#" + number;

									}
								
							}	
							
							function deleteRow(row){
								
								row.parentNode.parentNode.remove();
								
								var beforeRowLength = document.querySelectorAll("#edit2_edit_table .xsd-table .xsd-table-tr");
								
									for(var i = 0; i < beforeRowLength.length; i++){
										
										var number = i + 1;
										
										document.querySelectorAll("#edit2_edit_table .xsd-table .xsd-table-tr")[i].querySelector(".count-row").innerHTML = "#" + number;

									}
								
							}	

							function addNewRowXml(){
								
								var beforeRowLength = document.querySelectorAll("#edit3_edit_table .xml-table .xml-table-tr").length + 1;
								
								var row = "";
									
									row += "<tr class='xml-table-tr'>";
										row += "<td>";
											row += "<span class='count-row'>#"+beforeRowLength+"</span> <input type='text' name='FILE_NEW_XML_NAME' size='30' maxlength='500' value='' placeholder='Наименование прайс-листа'> ";
											row += "<input type='text' name='FILE_NEW_XML_FILE' size='30' maxlength='500' value='' placeholder='Путь к файлу на сервере'> ";
											row += "<input type='text' name='FILE_NEW_XML_FILE_LINK' size='30' maxlength='500' value='' placeholder='URL файла у поставщика'> ";
											row += "<select name='XSD_SCHEMA'><option value=''>Выбери XSD схему</option>";
												<?foreach($sellerXsdInfo as $item){?>
													row += "<option value='<?=$item['ID'];?>'><?=$item['FILE_XSD_NAME_SELLER'];?></option>";
												<?}?>
											row += "</select>";
											row += " <input type='button' name='browse' value='Выбрать / Загрузить прайс' OnClick='browseFile(this, \"priceList\");'> ";
											row += "<input type='button' name='deleteRow' value='Удалить строку' OnClick='deleteRowXml(this);'>";
										row += "</td>";
									row += "</tr>";
								
								document.querySelector("#edit3_edit_table .xml-table-tr-new").insertAdjacentHTML('beforeBegin', row);
								
							}

							function addNewRowXsd(){
								
								var beforeRowLength = document.querySelectorAll("#edit2_edit_table .xsd-table .xsd-table-tr").length + 1;
								
								var row = "";
									
									row += "<tr class='xsd-table-tr'>";
										row += "<td>";
											row += "<span class='count-row'>#"+beforeRowLength+"</span> <input type='text' name='FILE_NEW_XSD_NAME' size='30' maxlength='500' value='' placeholder='Наименование xsd-схемы'> ";
											row += "<input type='text' name='FILE_NEW_XSD_FILE' size='30' maxlength='500' value='' placeholder='Путь к файлу на сервере'> ";
											row += "<input type='button' name='browse' value='Выбрать / Загрузить файл' OnClick='browseFile(this, \"xsd\");'> ";
											row += "<input type='button' name='deleteRow' value='Удалить строку' OnClick='deleteRow(this);'>";
										row += "</td>";
									row += "</tr>";
								
								document.querySelector(".xsd-table-tr-new").insertAdjacentHTML('beforeBegin', row);
								
							}
				
							function saveXsdSeller(){
								
								var xsdInputTr = document.querySelectorAll("#edit2_edit_table .xsd-table .xsd-table-tr");
									
								var data = [];
									data["ID_SELLER"] = <?=$ID;?>;
									data["INPUT_XSD"] = [];
									
									for(var i = 0; i < xsdInputTr.length; i++){
										
										data["INPUT_XSD"][i] = [];

									}
									
									for(var i = 0; i < xsdInputTr.length; i++){
										
										data["INPUT_XSD"][i]["NAME_PRICE_XSD"] = xsdInputTr[i].querySelector("input[name='FILE_NEW_XSD_NAME']").value;
										data["INPUT_XSD"][i]["FILE_PRICE_XSD"] = xsdInputTr[i].querySelector("input[name='FILE_NEW_XSD_FILE']").value;

									}

								BX.ajax({
									url: '<?=$APPLICATION->GetCurPage(false);?>',
									data: {
										'type': 'saveXsdSeller',
										'data': data
									},
									method: 'POST',
									dataType: 'json',
									timeout: 30,
									async: true,
									processData: true,
									scriptsRunFirst: true,
									emulateOnload: true,
									start: true,
									cache: false,
									onsuccess: function(data){
										
										document.querySelector("#edit2_edit_table .errors-tab-2 ul").innerHTML = "";
										
										if(data["RESULT"]["ERRORS"].length > 0){
								
											var errorsHtml = "";
											
											for(error in data["RESULT"]["ERRORS"]){
												
												errorsHtml += "<li>" + data["RESULT"]["ERRORS"][error] + "</li>";
												
											}
											
											document.querySelector("#edit2_edit_table .errors-tab-2 ul").innerHTML = errorsHtml;
											
										}
										
										if(data["RESULT"]["ERRORS"].length == 0 && data["RESULT"]["RESULT"] == "Y"){
											
											window.location.href = "<?=$APPLICATION->GetCurUri();?>";
											
										}
										
									},
									onfailure: function(error){

										console.log(error);

									}
								}); 
								
							}
							
							function saveXmlSeller(){
								
								var xmlInputTr = document.querySelectorAll("#edit3_edit_table .xml-table .xml-table-tr");
									
								var data = [];
									data["ID_SELLER"] = <?=$ID;?>;
									data["INPUT_XML"] = [];
									
									for(var i = 0; i < xmlInputTr.length; i++){
										
										data["INPUT_XML"][i] = [];

									}
									
									for(var i = 0; i < xmlInputTr.length; i++){
										
										data["INPUT_XML"][i]["NAME_PRICE_XML"] = xmlInputTr[i].querySelector("input[name='FILE_NEW_XML_NAME']").value;
										data["INPUT_XML"][i]["FILE_PRICE_XML"] = xmlInputTr[i].querySelector("input[name='FILE_NEW_XML_FILE']").value;
										data["INPUT_XML"][i]["FILE_PRICE_XML_LINK"] = xmlInputTr[i].querySelector("input[name='FILE_NEW_XML_FILE_LINK']").value;
										data["INPUT_XML"][i]["XSD_SCHEME"] = xmlInputTr[i].querySelector("select[name='XSD_SCHEMA']").value;

									}
									
								BX.ajax({
									url: '<?=$APPLICATION->GetCurPage(false);?>',
									data: {
										'type': 'saveXmlSeller',
										'data': data
									},
									method: 'POST',
									dataType: 'json',
									timeout: 30,
									async: true,
									processData: true,
									scriptsRunFirst: true,
									emulateOnload: true,
									start: true,
									cache: false,
									onsuccess: function(data){
										
										document.querySelector("#edit3_edit_table .errors-tab-3 ul").innerHTML = "";
										
										if(data["RESULT"]["ERRORS"].length > 0){
								
											var errorsHtml = "";
											
											for(error in data["RESULT"]["ERRORS"]){
												
												errorsHtml += "<li>" + data["RESULT"]["ERRORS"][error] + "</li>";
												
											}
											
											document.querySelector("#edit3_edit_table .errors-tab-3 ul").innerHTML = errorsHtml;
											
										}
										
										if(data["RESULT"]["ERRORS"].length == 0 && data["RESULT"]["RESULT"] == "Y"){
											
											window.location.href = "<?=$APPLICATION->GetCurUri();?>";
											
										}
										
									},
									onfailure: function(error){

										console.log(error);

									}
								}); 
								
							}
							
							function getErrorsButton(idPriceList){
								
								var data = [];
									data["ID_PRICELIST"] = idPriceList;
									data["ID_SELLER"] = <?=$ID;?>;
								
								BX.ajax({
									url: '<?=$APPLICATION->GetCurPage(false);?>',
									data: {
										'type': 'getErrorsAnalize',
										'data': data
									},
									method: 'POST',
									dataType: 'json',
									timeout: 30,
									async: true,
									processData: true,
									scriptsRunFirst: true,
									emulateOnload: true,
									start: true,
									cache: false,
									onsuccess: function(data){
										
										var content = "<table style='width:100%;'>";
										
											var errors = JSON.parse(data["RESULT"]["RESULT"]);
											
											content += "<tr>";
													
												content += "<td>";
													
													content += "Текст ошибки";
														
												content += "</td>";
													
												content += "<td>";
														
													content += "Номер строки";
														
												content += "</td>";
													
											content += "</tr>";
											
											if(typeof errors.FATAL_ERROR !== "undefined"){
											
												content += "<tr>";
														
													content += "<td colspan='2'>";
														
														content += "Критические ошибки";
															
													content += "</td>";
														
												content += "</tr>";
												
												var i = 1;
												
												errors.FATAL_ERROR.forEach(function(element){
													
													content += "<tr>";
														
														content += "<td>";
															
															content += i + ". ";
															content += element.ERROR_TEXT;
															
														content += "</td>";
														
														content += "<td>";
															
															content += element.LINE;
															
														content += "</td>";
														
													content += "</tr>";
													
													i++;
													
												});
												
											}
											
											if(typeof errors.SCHEME_ERROR !== "undefined"){
											
												content += "<tr><td colspan='2'><br/></td></tr><tr>";
														
													content += "<td colspan='2'>";
														
														content += "Ошибки схемы";
															
													content += "</td>";
														
												content += "</tr>";
												
												var i = 1;
												
												errors.SCHEME_ERROR.forEach(function(element){
													
													content += "<tr>";
														
														content += "<td>";
															
															content += i + ". ";
															content += element.ERROR_TEXT;
															
														content += "</td>";
														
														content += "<td>";
															
															content += element.LINE;
															
														content += "</td>";
														
													content += "</tr>";
													
													i++;
													
												});
											
											}
										
										content += "</table>";

										var popup = new BX.CDialog({
										   'title': 'Список ошибок',
										   'content': content,
										   'draggable': true,
										   'resizable': true,
										   'buttons': [BX.CDialog.btnClose]
										});

										popup.Show();
										
									},
									onfailure: function(error){

										console.log(error);

									}
								});
								
							}
							
							function importSettings(idPriceList){
								
								var data = [];
									data["ID_PRICELIST"] = idPriceList;
								
								BX.ajax({
									url: '<?=$APPLICATION->GetCurPage(false);?>',
									data: {
										'type': 'getSettingsImportPriceList',
										'data': data
									},
									method: 'POST',
									dataType: 'json',
									timeout: 30,
									async: true,
									processData: true,
									scriptsRunFirst: true,
									emulateOnload: true,
									start: true,
									cache: false,
									onsuccess: function(data){
										
										var mainProperties = data["RESULT"]["RESULT"]["MAIN_PROPERTIES"];
										var otherProperties = data["RESULT"]["RESULT"]["OTHER_PROPERTIES"];
										
										var checked = "";
										
										var content = "";
										
											content += "<table class='popup-settings-table'>";
										
												content += "<tr>";
											
													content += "<td style='padding-right:20px; width:25%;'>";
											
														content += "Основные настройки";
											
													content += "</td>";
													
													content += "<td style='width:75%;'>";
											
														content += "Дополнительные настройки";
											
													content += "</td>";
											
												content += "</tr>";
												
												content += "<tr>";
											
													content += "<td>";
											
														content += "<div class='title-options'>Обновить/Добавить:</div>";
														
														content += "<ul class='main-option-list'>";
															
															if(mainProperties["NAME"] == "Y"){
																
																checked = "checked";
																
															}else{
																
																checked = "";
																
															}
															
															content += "<li><input class='adm-designed-checkbox' type='checkbox' name='IMPORT_MAIN_PROPERTIES[NAME]' value='Y' id='IMPORT_MAIN_PROPERTIES_NAME' "+checked+"><label class='adm-designed-checkbox-label' for='IMPORT_MAIN_PROPERTIES_NAME'>Название</label></li>";
															
															if(mainProperties["PHOTOS"] == "Y"){
																
																checked = "checked";
																
															}else{
																
																checked = "";
																
															}
															
															content += "<li><input class='adm-designed-checkbox' type='checkbox' name='IMPORT_MAIN_PROPERTIES[PHOTOS]' value='Y' id='IMPORT_MAIN_PROPERTIES_PHOTOS' "+checked+"><label class='adm-designed-checkbox-label' for='IMPORT_MAIN_PROPERTIES_PHOTOS'>Фотографии</label></li>";
															
															if(mainProperties["PRICE"] == "Y"){
																
																checked = "checked";
																
															}else{
																
																checked = "";
																
															}
															
															content += "<li><input class='adm-designed-checkbox' type='checkbox' name='IMPORT_MAIN_PROPERTIES[PRICE]' value='Y' id='IMPORT_MAIN_PROPERTIES_PRICE' "+checked+"><label class='adm-designed-checkbox-label' for='IMPORT_MAIN_PROPERTIES_PRICE'>Цена</label></li>";
															
															if(mainProperties["SKLAD"] == "Y"){
																
																checked = "checked";
																
															}else{
																
																checked = "";
																
															}
															
															content += "<li><input class='adm-designed-checkbox' type='checkbox' name='IMPORT_MAIN_PROPERTIES[SKLAD]' value='Y' id='IMPORT_MAIN_PROPERTIES_SKLAD' "+checked+"><label class='adm-designed-checkbox-label' for='IMPORT_MAIN_PROPERTIES_SKLAD'>Количество товара</label></li>";
															
															if(mainProperties["DESCRIPTION"] == "Y"){
																
																checked = "checked";
																
															}else{
																
																checked = "";
																
															}
															
															content += "<li><input class='adm-designed-checkbox' type='checkbox' name='IMPORT_MAIN_PROPERTIES[DESCRIPTION]' value='Y' id='IMPORT_MAIN_PROPERTIES_DESCRIPTION' "+checked+"><label class='adm-designed-checkbox-label' for='IMPORT_MAIN_PROPERTIES_DESCRIPTION'>Описание</label></li>";
															
															if(mainProperties["PROPERTIES"] == "Y"){
																
																checked = "checked";
																
															}else{
																
																checked = "";
																
															}
															
															content += "<li><input class='adm-designed-checkbox' type='checkbox' name='IMPORT_MAIN_PROPERTIES[PROPERTIES]' value='Y' id='IMPORT_MAIN_PROPERTIES_PROPERTIES' "+checked+"><label class='adm-designed-checkbox-label' for='IMPORT_MAIN_PROPERTIES_PROPERTIES'>Свойства</label></li>";
											
														content += "</ul>";
														
													content += "</td>";
													
													content += "<td>";
														
														content += "<div class='title-options'>Не выгружать на площадки:</div>";
														
														content += "<ul class='other-option-list other-option-list-dont-export-stores'>";
														
															for(store in otherProperties["DONT_EXPORT_PLATFORM"]){
														
																if(otherProperties["DONT_EXPORT_PLATFORM"][store] == "Y"){
																
																	checked = "checked";
																	
																}else{
																	
																	checked = "";
																	
																}
														
																content += "<li><input class='adm-designed-checkbox' type='checkbox' name='IMPORT_OTHER_PROPERTIES[DONT_EXPORT_PLATFORM]["+store+"]' value='Y' id='IMPORT_OTHER_PROPERTIES_"+store+"' "+checked+"><label class='adm-designed-checkbox-label' for='IMPORT_OTHER_PROPERTIES_"+store+"'>"+store+"</label></li>";
														
															}
														
														content += "</ul>";
														
														content += "<br/>";
														
														content += "<div class='title-options'>Статус товаров, которых нет в наличии при добавлении новых позиций:</div>";
														
														content += "<ul class='other-option-list'>";

															if(otherProperties["AVAILABLE"]["NEW_PRODUCT"] == "Y"){
																
																content += "<li><input type='radio' name='IMPORT_OTHER_PROPERTIES[AVAILABLE][NEW_PRODUCT]' value='Y' id='IMPORT_OTHER_PROPERTIES_NEW_PRODUCT_Y' checked><label for='IMPORT_OTHER_PROPERTIES_NEW_PRODUCT_Y'>Добавить</label></li>";
															
																content += "<li><input type='radio' name='IMPORT_OTHER_PROPERTIES[AVAILABLE][NEW_PRODUCT]' value='N' id='IMPORT_OTHER_PROPERTIES_NEW_PRODUCT_N'><label for='IMPORT_OTHER_PROPERTIES_NEW_PRODUCT_N'>Не добавлять</label></li>";
																
															}

															if(otherProperties["AVAILABLE"]["NEW_PRODUCT"] == "N"){
																
																content += "<li><input type='radio' name='IMPORT_OTHER_PROPERTIES[AVAILABLE][NEW_PRODUCT]' value='Y' id='IMPORT_OTHER_PROPERTIES_NEW_PRODUCT_Y'><label for='IMPORT_OTHER_PROPERTIES_NEW_PRODUCT_Y'>Добавить</label></li>";
															
																content += "<li><input type='radio' name='IMPORT_OTHER_PROPERTIES[AVAILABLE][NEW_PRODUCT]' value='N' id='IMPORT_OTHER_PROPERTIES_NEW_PRODUCT_N' checked><label for='IMPORT_OTHER_PROPERTIES_NEW_PRODUCT_N'>Не добавлять</label></li>";
																
															}
														
														content += "</ul>";
														
														content += "<br/>";
														
														content += "<div class='title-options'>Статус товаров, которые отсутствуют в файле при обновлении:</div>";
														
														content += "<ul class='other-option-list'>";

															if(otherProperties["AVAILABLE"]["UPDATE_PRODUCT"] == "DELETE_EXPORT"){

																content += "<li><input type='radio' name='IMPORT_OTHER_PROPERTIES[AVAILABLE][UPDATE_PRODUCT]' value='DELETE_EXPORT' id='IMPORT_OTHER_PROPERTIES_UPDATE_PRODUCT_DELETE_EXPORT' checked><label for='IMPORT_OTHER_PROPERTIES_UPDATE_PRODUCT_DELETE_EXPORT'>Удалить(Установка статуса \"Нет в наличии\" на демпинге и удаление товаров из файлов выгрузок на площадки)</label></li>";
																
																content += "<li><input type='radio' name='IMPORT_OTHER_PROPERTIES[AVAILABLE][UPDATE_PRODUCT]' value='DELETE_ALL' id='IMPORT_OTHER_PROPERTIES_UPDATE_PRODUCT_DELETE_ALL'><label for='IMPORT_OTHER_PROPERTIES_UPDATE_PRODUCT_DELETE_ALL'>Удалить полностью(Удаление с сайта и удаление из всех файлов выгрузок)</label></li>";
															
															}
															
															if(otherProperties["AVAILABLE"]["UPDATE_PRODUCT"] == "DELETE_ALL"){

																content += "<li><input type='radio' name='IMPORT_OTHER_PROPERTIES[AVAILABLE][UPDATE_PRODUCT]' value='DELETE_EXPORT' id='IMPORT_OTHER_PROPERTIES_UPDATE_PRODUCT_DELETE_EXPORT'><label for='IMPORT_OTHER_PROPERTIES_UPDATE_PRODUCT_DELETE_EXPORT'>Удалить(Установка статуса \"Нет в наличии\" на демпинге и удаление товаров из файлов выгрузок на площадки)</label></li>";
																
																content += "<li><input type='radio' name='IMPORT_OTHER_PROPERTIES[AVAILABLE][UPDATE_PRODUCT]' value='DELETE_ALL' id='IMPORT_OTHER_PROPERTIES_UPDATE_PRODUCT_DELETE_ALL' checked><label for='IMPORT_OTHER_PROPERTIES_UPDATE_PRODUCT_DELETE_ALL'>Удалить полностью(Удаление с сайта и удаление из всех файлов выгрузок)</label></li>";
															
															}
														
														content += "</ul>";
														
														content += "<br/>";
														
														content += "<div class='title-options'>Наценка на демпинге(считается от исходной цены прайс-листа):</div>";
														
														content += "<ul class='other-option-list'>";

															content += "<li>";
															
															content += "<input type='text' name='IMPORT_OTHER_PROPERTIES[PRICE]' value='"+otherProperties["PRICE"]["PRICE_UPDATE"]+"' id='IMPORT_OTHER_PROPERTIES_PRICE'>";
															
															content += "<select name='IMPORT_OTHER_PROPERTIES[PRICE][TYPE]'>";
																
																content += "<option value='1' ";
																
																	if(otherProperties["PRICE"]["PRICE_TYPE"] == 1){content += "selected";}
																
																content += ">%</option>";
																
																content += "<option value='2' ";
																
																	if(otherProperties["PRICE"]["PRICE_TYPE"] == 2){content += "selected";}
																
																content += ">Число</option>";
																
															content += "</select>";
															
															content += "</li>";
															
														content += "</ul>";
														
														content += "<br/>";
														
														content += "<div class='title-options'>Наценка на площадках(считается от исходной цены прайс-листа):</div>";
														
															content += "<ul class='other-option-list get-store-prices'>";
														
																for(store in otherProperties["PRICES"]){
															
																	content += "<li>";
																			
																	content += store + ": ";
																		
																	content += "<input type='text' name='IMPORT_OTHER_PROPERTIES[PRICES]["+store+"][VALUE]' value='"+otherProperties["PRICES"][store]["VALUE"]+"' id='IMPORT_OTHER_PROPERTIES_PRICES_"+store+"'>";
																			
																	content += "<select name='IMPORT_OTHER_PROPERTIES[PRICES]["+store+"][TYPE]'>";
																				
																		content += "<option value='1' ";
																					
																		if(otherProperties["PRICES"][store]["TYPE"] == "1"){content += "selected";}
																					
																		content += ">%</option>";
																					
																		content += "<option value='2' ";
																					
																		if(otherProperties["PRICES"][store]["TYPE"] == "2"){content += "selected";}
																					
																		content += ">Число</option>";
																				
																	content += "</select>";
																			
																	content += "</li>";
															
																}
															
															content += "</ul>";
															
															content += "<br/>";
														
															content += "<div class='title-options' style='display:none;'>Символ разделителя множественного свойства в прайс-листе:</div>";
															
															content += "<ul class='other-option-list' style='display:none;'>";

																content += "<li><input type='text' name='IMPORT_OTHER_PROPERTIES[DELIMETER]' value='"+otherProperties["DELIMETER"]+"' id='IMPORT_OTHER_PROPERTIES_DELIMETER'></li>";

															content += "</ul>";
														
														content += "<div class='title-options'>Добавить к названию товара:</div>";
														
														content += "<select name='IMPORT_OTHER_PROPERTIES[ADD_NAME]'>";
																
															content += "<option value='0' ";
																
																if(otherProperties["ADD_NAME"] == '0'){content += "selected";}
																
															content += ">Ничего не добавлять</option>";
															
															content += "<option value='1' ";
																
																if(otherProperties["ADD_NAME"] == 1){content += "selected";}
																
															content += ">Случайное число (5 символов)</option>";
															
															content += "<option value='2' ";
																
																if(otherProperties["ADD_NAME"] == 2){content += "selected";}
																
															content += ">ID торгового предложения</option>";
																
														content += "</select>";
														
													content += "</td>";
											
												content += "</tr>";

											content += "</table>";
											
											content += '<div class="errors-tab-5"><ul></ul></div>';
											
											content += "<br/>";
											
											content += "<input type='button' name='SAVE_PROPERTIES_SETTINGS' value='Сохранить настройки' onclick='savePropertiesSettings("+idPriceList+");' id='SAVE_PROPERTIES_SETTINGS'>";
										
										var popup = new BX.CDialog({
										   'title': 'Настройки',
										   'content': content,
										   'draggable': true,
										   'resizable': true,
										   'buttons': [BX.CDialog.btnClose]
										});

										BX.addCustomEvent(popup, 'onWindowRegister',function(){  
											this.DIV.className = "bx-core-window bx-core-adm-dialog SETTINGS_PRICE_LIST";
										});

										popup.Show();
										
										BX.addCustomEvent(popup, 'onWindowClose',function(){  
											
											document.querySelector(".SETTINGS_PRICE_LIST").remove();
											document.querySelector(".bx-core-dialog-overlay").remove();
											
										});
										
									},
									onfailure: function(error){

										console.log(error);

									}
								});
								
							}
							
							function savePropertiesSettings(idPriceList){
								
								var propertiesList = document.querySelectorAll(".main-option-list input");
								
								var otherPropertiesListDontExportStore = document.querySelectorAll(".other-option-list.other-option-list-dont-export-stores input");
								
								var otherPropertiesListAvailableNewProduct = document.querySelector("#IMPORT_OTHER_PROPERTIES_NEW_PRODUCT_Y");
								
								var otherPropertiesListAvailableUpdateProduct = document.querySelector("#IMPORT_OTHER_PROPERTIES_UPDATE_PRODUCT_DELETE_EXPORT");
								
								var priceValue = document.querySelector("input[name='IMPORT_OTHER_PROPERTIES[PRICE]']");
								var priceType = document.querySelector("select[name='IMPORT_OTHER_PROPERTIES[PRICE][TYPE]']");
								var addName = document.querySelector("select[name='IMPORT_OTHER_PROPERTIES[ADD_NAME]']");
								
								
								var getStorePrices = document.querySelectorAll(".get-store-prices input");
								var getStorePricesType = document.querySelectorAll(".get-store-prices select");
								
								var nameOptionResult = [];
									nameOptionResult["MAIN_PROPERTIES"] = [];
									nameOptionResult["OTHER_PROPERTIES"] = [];
									nameOptionResult["OTHER_PROPERTIES"]["PRICES"] = [];
										
								for(i = 0; i < getStorePrices.length; i++){
									
									var nameResult = getStorePrices[i].name.replace('IMPORT_OTHER_PROPERTIES[PRICES][', '');
										nameResult = nameResult.replace('][VALUE]', '');
										
									nameOptionResult["OTHER_PROPERTIES"]["PRICES"][nameResult] = [];
									
								}
								
								for(i = 0; i < getStorePrices.length; i++){
									
									var nameResult = getStorePrices[i].name.replace('IMPORT_OTHER_PROPERTIES[PRICES][', '');
										nameResult = nameResult.replace('][VALUE]', '');
										
									nameOptionResult["OTHER_PROPERTIES"]["PRICES"][nameResult]["VALUE"] = getStorePrices[i].value;
									nameOptionResult["OTHER_PROPERTIES"]["PRICES"][nameResult]["TYPE"] = getStorePricesType[i].options[getStorePricesType[i].selectedIndex].value;
									
								}
									
								for(i = 0; i < propertiesList.length; i++){
									
									var nameResult = propertiesList[i].name.replace('IMPORT_MAIN_PROPERTIES[', '');
										nameResult = nameResult.replace(']', '');
									
									nameOptionResult["MAIN_PROPERTIES"][nameResult] = propertiesList[i].checked;
									
								}
								
								for(i = 0; i < otherPropertiesListDontExportStore.length; i++) {
									
									var nameResultMain = otherPropertiesListDontExportStore[i].name.split('[');
										nameResult = nameResultMain[1].replace(']', '');
										platformName = nameResultMain[2].replace(']', '');
									
									nameOptionResult["OTHER_PROPERTIES"][nameResult] = [];								
									
								}
								
								for(i = 0; i < otherPropertiesListDontExportStore.length; i++) {
									
									var checked = "";
									
									var nameResultMain = otherPropertiesListDontExportStore[i].name.split('[');
										nameResult = nameResultMain[1].replace(']', '');
										platformName = nameResultMain[2].replace(']', '');
										
									if(otherPropertiesListDontExportStore[i].checked){
										
										checked = "Y";
										
									}else{
										
										checked = "N";
										
									}

									nameOptionResult["OTHER_PROPERTIES"][nameResult][platformName] = checked;
									
								}
								
								nameOptionResult["OTHER_PROPERTIES"]["AVAILABLE"] = [];
								
								if(otherPropertiesListAvailableNewProduct.checked){
									
									nameOptionResult["OTHER_PROPERTIES"]["AVAILABLE"]["NEW_PRODUCT"] = "Y";
									
								}else{
									
									nameOptionResult["OTHER_PROPERTIES"]["AVAILABLE"]["NEW_PRODUCT"] = "N";
									
								}
								
								if(otherPropertiesListAvailableUpdateProduct.checked){
									
									nameOptionResult["OTHER_PROPERTIES"]["AVAILABLE"]["UPDATE_PRODUCT"] = "DELETE_EXPORT";
									
								}else{
									
									nameOptionResult["OTHER_PROPERTIES"]["AVAILABLE"]["UPDATE_PRODUCT"] = "DELETE_ALL";
									
								}
								
								nameOptionResult["OTHER_PROPERTIES"]["PRICE"] = [];
								
								nameOptionResult["OTHER_PROPERTIES"]["PRICE"]["PRICE_UPDATE"] = priceValue.value;
								nameOptionResult["OTHER_PROPERTIES"]["PRICE"]["PRICE_TYPE"] = priceType.options[priceType.selectedIndex].value;
								
								nameOptionResult["OTHER_PROPERTIES"]["ADD_NAME"] = addName.options[addName.selectedIndex].value;

								var delimeter = document.querySelector("input[name='IMPORT_OTHER_PROPERTIES[DELIMETER]']");

								nameOptionResult["OTHER_PROPERTIES"]["DELIMETER"] = delimeter.value;

								var deleteXml = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteXml']");
								
								var importButtonIsset = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importPriceList']");
									
								var data = [];
									data["ID_PRICELIST"] = idPriceList;
									data["ID_SELLER"] = <?=$ID;?>;
									data["PROPERTIES"] = nameOptionResult;
								
								BX.ajax({
									url: '<?=$APPLICATION->GetCurPage(false);?>',
									data: {
										'type': 'savePropertiesSettings',
										'data': data
									},
									method: 'POST',
									dataType: 'json',
									timeout: 30,
									async: true,
									processData: true,
									scriptsRunFirst: true,
									emulateOnload: true,
									start: true,
									cache: false,
									onsuccess: function(data){
										
										document.querySelector(".errors-tab-5 ul").innerHTML = "";
										
										if(data["RESULT"]["RESULT"] == "Y" && data["RESULT"]["ERRORS"].length == 0){
											
											document.querySelector(".SETTINGS_PRICE_LIST").remove();
											document.querySelector(".bx-core-dialog-overlay").remove();
											
											if(importButtonIsset == null){
											
												var importButton = document.createElement("input");
													importButton.setAttribute("type", "button");
													importButton.setAttribute("name", "importPriceList");
													importButton.setAttribute("value", "Импортировать");
													importButton.setAttribute("onclick", "importPriceList("+ idPriceList +")");

												deleteXml.after(importButton);
											
											}
											
										}
										
										if(data["RESULT"]["RESULT"] == "N" && data["RESULT"]["ERRORS"].length > 0){
								
											var errorsHtml = "";
											
											for(error in data["RESULT"]["ERRORS"]){
												
												errorsHtml += "<li>" + data["RESULT"]["ERRORS"][error] + "</li>";
												
											}
											
											document.querySelector(".errors-tab-5 ul").innerHTML = errorsHtml;
											
										}
										
									},
									onfailure: function(error){

										console.log(error);

									}
								});
								
							}
							
							function importPriceList(idPriceList){
								
								var importSettings = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importSettings']");
								
								var thisDeleteButtonInTableXmlPrice = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteXml']");

								var importPriceList = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importPriceList']");
								
								importSettings.disabled = true;
								thisDeleteButtonInTableXmlPrice.disabled = true;
								importPriceList.disabled = true;
								
								var data = [];
									data["ID_PRICELIST"] = idPriceList;
									data["ID_SELLER"] = <?=$ID;?>;
								
								BX.ajax({
									url: '<?=$APPLICATION->GetCurPage(false);?>',
									data: {
										'type': 'startImportPriceList',
										'data': data
									},
									method: 'POST',
									dataType: 'json',
									timeout: 30,
									async: true,
									processData: true,
									scriptsRunFirst: true,
									emulateOnload: true,
									start: true,
									cache: false,
									onsuccess: function(data){
										
										document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " .errors-tab-3-1 ul").innerHTML = "";
										
										var notesHtml = "";
										
										if(data["RESULT"] == "Y" && data["ERRORS"].length == 0){
											
											timerToImport[idPriceList] = setInterval(getResultImport, 1000, idPriceList, data["LAST_ID_IMPORT"]);
											
											var cancelButton = document.createElement("input");
												cancelButton.setAttribute("type", "button");
												cancelButton.setAttribute("name", "cancelImport");
												cancelButton.setAttribute("value", "Отменить импорт");
												cancelButton.setAttribute("onclick", "cancelImportPriceList("+ idPriceList +")");

												importPriceList.after(cancelButton);
											
											notesHtml += "<li data-status='1'>Прайс-лист успешно установлен в очередь на парсинг</li>";
											
											importPriceList.value = "Ждём завершения импорта прайс-листа";
											
										}
										
										if(data["ERRORS"].length > 0){
								
											for(error in data["ERRORS"]){
												
												notesHtml += "<li>" + data["ERRORS"][error] + "</li>";
												
											}
											
										}
										
										document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " .errors-tab-3-1 ul").innerHTML = notesHtml;
										
									},
									onfailure: function(error){

										console.log(error);

									}
								});
								
							}
							
							function getResultImport(idPriceList, lastIdImport){
								
								var notesHtmlDiv = document.querySelector(".xml-table-tr-main-table-id-"+idPriceList+" .errors-tab-3-1 ul");
								
								var importSettings = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importSettings']");
								
								var thisDeleteButtonInTableXmlPrice = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteXml']");
								
								var importPriceList = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importPriceList']");
								
								var cancelImport = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='cancelImport']");
								
								var data = [];
									data["ID_PRICELIST"] = idPriceList;
									data["LAST_ID_IMPORT"] = lastIdImport;
								
								BX.ajax({
									url: '<?=$APPLICATION->GetCurPage(false);?>',
									data: {
										'type': 'getResultImport',
										'data': data
									},
									method: 'POST',
									dataType: 'json',
									timeout: 30,
									async: true,
									processData: true,
									scriptsRunFirst: true,
									emulateOnload: true,
									start: true,
									cache: false,
									onsuccess: function(data){
										
										notesHtmlDiv.innerHTML = "";
										
										console.log(data);
										
										for(result in data["RESULT"]["RESULT"]){
											
											if(
												data["RESULT"]["RESULT"][result]["TASK"] == "START_IMPORT" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "0" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "ANALIZE_PRICE_LIST" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "1" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "ANALIZE_PRICE_LIST" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "2" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "GET_PRODUCTS_TO_BASE" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "1" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "GET_PRODUCTS_TO_BASE" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "2" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "INSERT_AND_UPDATE_PRODUCT" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "1" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "INSERT_AND_UPDATE_PRODUCT" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "2" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "INSERT_AND_UPDATE_PRODUCT" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "3" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "IMPORT_UPDATE_PICTURE" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "0" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "IMPORT_UPDATE_PICTURE" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "1" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "GET_IMPORT_UPDATE_PICTURE" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "0" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "GET_IMPORT_UPDATE_PICTURE" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "1" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "GET_IMPORT_UPDATE_PICTURE" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "2" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "EXPORT_PRICE_LIST" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "0" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "EXPORT_PRICE_LIST" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "1" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "EXPORT_PRICE_LIST" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "2" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "DELETE_MISSING_ITEMS" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "0" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "DELETE_MISSING_ITEMS" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "1" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
											){
												
												var li = document.createElement("li");
													li.setAttribute("data-status-import", data["RESULT"]["RESULT"][result]["TASK"]);
													li.append(data["RESULT"]["RESULT"][result]["TEXT_IMPORT"]);
														
												notesHtmlDiv.append(li);
												
											}
											
											// Если были найдены ошибки при импорте прайс-листа
											if(
												data["RESULT"]["RESULT"][result]["TASK"] == "ANALIZE_PRICE_LIST" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "2" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "N" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
											){
												
												clearInterval(timerToImport[idPriceList]);
												
												importSettings.disabled = false;
												thisDeleteButtonInTableXmlPrice.disabled = false;
												importPriceList.disabled = false;
												importPriceList.value = "Импортировать";
												
												if(cancelImport != undefined){
													
													cancelImport.remove();
													
												}
												
												var li = document.createElement("li");
													li.setAttribute("data-status-import", data["RESULT"]["RESULT"][result]["TASK"]);
													li.append(data["RESULT"]["RESULT"][result]["TEXT_IMPORT"]);
													
													var showErrorButton = document.createElement("input");
														showErrorButton.setAttribute("type", "button");
														showErrorButton.setAttribute("name", "analizeErrorButton");
														showErrorButton.setAttribute("value", "Просмотреть ошибки");
														showErrorButton.setAttribute("onclick", "getErrorsButton("+ idPriceList +")");
														
													li.insertAdjacentElement("beforeEnd", showErrorButton);
													
												notesHtmlDiv.append(li);
												
											}
											
											// Если произошла ошибка при добавлении товаров из прайс-листа в БД
											if(
												data["RESULT"]["RESULT"][result]["TASK"] == "ANALIZE_PRICE_LIST" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "1" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "N" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "GET_PRODUCTS_TO_BASE" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "1" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "N" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
												||
												data["RESULT"]["RESULT"][result]["TASK"] == "DELETE_MISSING_ITEMS" && 
												data["RESULT"]["RESULT"][result]["STEP"] == "2" && 
												data["RESULT"]["RESULT"][result]["RESULT_STEP"] == "Y" &&
												data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "N"
											){
												
												clearInterval(timerToImport[idPriceList]);
												
												importSettings.disabled = false;
												thisDeleteButtonInTableXmlPrice.disabled = false;
												importPriceList.disabled = false;
												importPriceList.value = "Импортировать";
												
												if(cancelImport != undefined){
													
													cancelImport.remove();
													
												}
												
												var li = document.createElement("li");
													li.setAttribute("data-status-import", data["RESULT"]["RESULT"][result]["TASK"]);
													li.append(data["RESULT"]["RESULT"][result]["TEXT_IMPORT"]);
													
												notesHtmlDiv.append(li);
												
											}
											
										}
										
									},
									onfailure: function(error){

										console.log(error);

									}
								});
								
							}
							
							var cancelImportTimer = [];
							
							function cancelImportPriceList(idPriceList){
								
								var notesHtmlDiv = document.querySelector(".xml-table-tr-main-table-id-"+idPriceList+" .errors-tab-3-1 ul");
								
								var thisDeleteButtonInTableXmlPrice = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteXml']");
								
								//var matchPropertyButton = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='matchPropertyButton']");
								
								var importSettings = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importSettings']");
								
								var importPriceList = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importPriceList']");
								
								//var deleteProductThisPriceList = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteProductThisPriceList']");
								
								var cancelImport = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='cancelImport']");

								var data = [];
									data["ID_PRICELIST"] = idPriceList;
									data["ID_SELLER"] = <?=$ID;?>;
								
								BX.ajax({
									url: '<?=$APPLICATION->GetCurPage(false);?>',
									data: {
										'type': 'cancelImportPriceList',
										'data': data
									},
									method: 'POST',
									dataType: 'json',
									timeout: 30,
									async: true,
									processData: true,
									scriptsRunFirst: true,
									emulateOnload: true,
									start: true,
									cache: false,
									onsuccess: function(data){
										
										if(data["RESULT"]["RESULT"] == "Y" && data["ERRORS"].length == 0){
											
											clearInterval(timerToImport[idPriceList]);
											
											
											var li = document.createElement("li");
												li.setAttribute("data-status-import", "CANCEL_IMPORT");
												li.append("Импорт отменён");

											notesHtmlDiv.append(li);
											
											importSettings.disabled = false;
											thisDeleteButtonInTableXmlPrice.disabled = false;
											importPriceList.disabled = false;
											importPriceList.value = "Импортировать";
											
											cancelImport.remove();
											
											//cancelImportTimer[idPriceList] = setInterval(getResultCancelImport, 3000, idPriceList, data["RESULT"]["LAST_ID_IMPORT"]);
											
										}
										
										if(data["ERRORS"].length > 0){
								
											clearInterval(cancelImportTimer[idPriceList]);
											
											var li = document.createElement("li");
												li.setAttribute("data-status-import", "FATAL_ERROR");
												
												for(error in data["RESULT"]["ERRORS"]){
												
													li.append(data["RESULT"]["RESULT"][error]);
														
												}		
												
											notesHtmlDiv.append(li);
											
										}
										
									},
									onfailure: function(error){

										console.log(error);

									}
								});

							}
							
							function savePropertiesTimeSettings(idPriceList){
								
								var thisDeleteButtonInTableXmlPrice = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteXml']");
								
								var importSettings = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importSettings']");
								
								var importPriceList = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importPriceList']");
								
								var apply = confirm("Внимание! При сохранении настроек происходит старт установки таймера(в случае, если чекнуто свойство 'Активность'). Вручную нажать кнопку 'Импортировать', пока активен таймер будет невозможно. Только автоимпорт. Старт автообновления начнётся через выбранный промежуток времени(в случае, если чекнуто свойство 'Активность'). \r Продолжаем ?");
									
								if(apply){
								
									var propertiesActive = document.getElementById("TIME_SETTINGS_ACTIVE");
									var propertiesTime = document.querySelector("select[name='TIME_SETTINGS_TIMER']");
											
									var data = [];
										data["ID_PRICELIST"] = idPriceList;
										data["ID_SELLER"] = <?=$ID;?>;
										data["ACTIVE"] = propertiesActive.checked;
										data["TIME_SETTING"] = propertiesTime.value;

									BX.ajax({
										url: '<?=$APPLICATION->GetCurPage(false);?>',
										data: {
											'type': 'savePropertiesTimeSettings',
											'data': data
										},
										method: 'POST',
										dataType: 'json',
										timeout: 30,
										async: true,
										processData: true,
										scriptsRunFirst: true,
										emulateOnload: true,
										start: true,
										cache: false,
										onsuccess: function(data){

											document.querySelector(".errors-tab-5 ul").innerHTML = "";
												
											if(data["RESULT"]["RESULT"] == "Y" && data["RESULT"]["ERRORS"].length == 0){
												
												if(propertiesActive.checked == true){
													
													thisDeleteButtonInTableXmlPrice.disabled = true;
													importSettings.disabled = true;
													importPriceList.disabled = true;
													
												}else{
													
													thisDeleteButtonInTableXmlPrice.disabled = false;
													importSettings.disabled = false;
													importPriceList.disabled = false;
													
												}
												
												document.querySelector(".SETTINGS_TIME_PRICE_LIST").remove();
												document.querySelector(".bx-core-dialog-overlay").remove();
													
											}
												
											if(data["RESULT"]["ERRORS"].length > 0){
										
												var errorsHtml = "";
													
												for(error in data["RESULT"]["ERRORS"]){
														
													errorsHtml += "<li>" + data["RESULT"]["ERRORS"][error] + "</li>";
														
												}
													
												document.querySelector(".errors-tab-5 ul").innerHTML = errorsHtml;
													
											}
												
										},
										onfailure: function(error){

											console.log(error);

										}
									});
										
								}
								
							}
							
							function timeXml(idPriceList){
								
								var data = [];
									data["ID_PRICELIST"] = idPriceList;
									data["ID_SELLER"] = <?=$ID;?>;
								
								BX.ajax({
									url: '<?=$APPLICATION->GetCurPage(false);?>',
									data: {
										'type': 'getSettingsTimeXml',
										'data': data
									},
									method: 'POST',
									dataType: 'json',
									timeout: 30,
									async: true,
									processData: true,
									scriptsRunFirst: true,
									emulateOnload: true,
									start: true,
									cache: false,
									onsuccess: function(data){
										
										//console.log(data);
										
										var checked = "";
										
										if(data["RESULT"]["ACTIVE"] == "Y"){
											
											checked = " checked";
											
										}
										
										var content = "";
										
											content += "<table class='popup-settings-table'>";
										
												content += "<tr>";
													
													content += "<td>";
														
														content += "<ul class='main-option-list'>";
														
															content += "<li>";
																
																content += "<input class='adm-designed-checkbox' type='checkbox' name='TIME_SETTINGS_ACTIVE' value='Y' id='TIME_SETTINGS_ACTIVE'"+checked+">";
																content += "<label class='adm-designed-checkbox-label' for='TIME_SETTINGS_ACTIVE'>Активность</label>";
															
															content += "</li>";
															
														content += "</ul>";
														
														content += "<div class='title-options'>Автообновление прайс-листа каждые: ";
														
															content += "<select name='TIME_SETTINGS_TIMER'>";
																
																content += "<option value=''>Выбрать</option>";
																
																content += "<option value='300' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 300){content += "selected";}
																	
																content += ">5 минут</option>";
																	
																content += "<option value='600' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 600){content += "selected";}
																	
																content += ">10 минут</option>";
																	
																content += "<option value='900' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 900){content += "selected";}
																	
																content += ">15 минут</option>";
																
																content += "<option value='1200' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 1200){content += "selected";}
																	
																content += ">20 минут</option>";
																
																content += "<option value='1500' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 1500){content += "selected";}
																	
																content += ">25 минут</option>";
																
																content += "<option value='1800' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 1800){content += "selected";}
																	
																content += ">30 минут</option>";
																
																content += "<option value='2100' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 2100){content += "selected";}
																	
																content += ">35 минут</option>";
																
																content += "<option value='2400' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 2400){content += "selected";}
																	
																content += ">40 минут</option>";
																
																content += "<option value='2700' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 2700){content += "selected";}
																	
																content += ">45 минут</option>";
																
																content += "<option value='3000' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 3000){content += "selected";}
																	
																content += ">50 минут</option>";
																
																content += "<option value='3300' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 3300){content += "selected";}
																	
																content += ">55 минут</option>";
																
																content += "<option value='3600' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 3600){content += "selected";}
																	
																content += ">1 час</option>";
																
																content += "<option value='4500' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 4500){content += "selected";}
																	
																content += ">1 час 15 минут</option>";
																
																content += "<option value='5400' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 5400){content += "selected";}
																	
																content += ">1 час 30 минут</option>";
																
																content += "<option value='6300' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 6300){content += "selected";}
																	
																content += ">1 час 45 минут</option>";
																
																content += "<option value='7200' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 7200){content += "selected";}
																	
																content += ">2 часа</option>";
																
																content += "<option value='10800' ";
																	
																	if(data["RESULT"]["TIME_SETTING"] == 10800){content += "selected";}
																	
																content += ">3 часа</option>";
																	
															content += "</select>";
														
														content += "</div>";
														
													content += "</td>";
													
												content += "</tr>";

											content += "</table>";
											
											content += '<div class="errors-tab-5"><ul></ul></div>';
											
											content += "<input type='button' name='SAVE_PROPERTIES_TIME_SETTINGS' value='Сохранить настройки' onclick='savePropertiesTimeSettings("+idPriceList+");' id='SAVE_PROPERTIES_TIME_SETTINGS'>";
										
										var popup = new BX.CDialog({
										   'title': 'Настройка расписания импорта',
										   'content': content,
										   'draggable': true,
										   'resizable': true,
										   'buttons': [BX.CDialog.btnClose]
										});

										BX.addCustomEvent(popup, 'onWindowRegister',function(){
											this.DIV.className = "bx-core-window bx-core-adm-dialog SETTINGS_TIME_PRICE_LIST";
										});

										popup.Show();
										
										BX.addCustomEvent(popup, 'onWindowClose',function(){  
											
											document.querySelector(".SETTINGS_TIME_PRICE_LIST").remove();
											document.querySelector(".bx-core-dialog-overlay").remove();
											
										});
										
									},
									onfailure: function(error){

										console.log(error);

									}
								});
								
							}
							
							/*
							function getResultCancelImport(idPriceList, lastIdImport){
								
								var notesHtmlDiv = document.querySelector(".xml-table-tr-main-table-id-"+idPriceList+" .errors-tab-3-1 ul");
								
								var thisDeleteButtonInTableXmlPrice = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteXml']");
								
								var importSettings = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importSettings']");
								
								var importPriceList = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='importPriceList']");
								
								var deleteProductThisPriceList = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='deleteProductThisPriceList']");
								
								var cancelImport = document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " input[name='cancelImport']");
								
								var data = [];
									data["ID_PRICELIST"] = idPriceList;
									data["LAST_ID_IMPORT"] = lastIdImport;
								
								BX.ajax({
									url: '<?=$APPLICATION->GetCurPage(false);?>',
									data: {
										'type': 'getResultCancelImport',
										'data': data
									},
									method: 'POST',
									dataType: 'json',
									timeout: 30,
									async: true,
									processData: true,
									scriptsRunFirst: true,
									emulateOnload: true,
									start: true,
									cache: false,
									onsuccess: function(data){
										
										for(result in data["RESULT"]["RESULT"]){
											
											if(data["RESULT"]["RESULT"][result]["STATUS_IMPORT"] == "0" && data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "Y"){
												
												if(document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " .errors-tab-3-1 ul li[data-status-import='"+data["RESULT"]["RESULT"][result]["STATUS_IMPORT"]+"']") == null){
												
													var li = document.createElement("li");
														li.setAttribute("data-status-import", data["RESULT"]["RESULT"][result]["STATUS_IMPORT"]);
														li.append(data["RESULT"]["RESULT"][result]["TEXT_IMPORT"]);
														
													notesHtmlDiv.append(li);	
												
												}
												
											}
											
											if(data["RESULT"]["RESULT"][result]["STATUS_IMPORT"] == "100" && data["RESULT"]["RESULT"][result]["CANCEL_IMPORT"] == "Y"){
												
												clearInterval(cancelImportTimer[idPriceList]);
												
												thisDeleteButtonInTableXmlPrice.disabled = false;
												importSettings.disabled = false;
												importPriceList.value = "Импортировать";
												importPriceList.disabled = false;
												deleteProductThisPriceList.disabled = false;

												cancelImport.remove();
												
												if(document.querySelector(".xml-table-tr-main-table-id-" + idPriceList + " .errors-tab-3-1 ul li[data-status-import='"+data["RESULT"]["RESULT"][result]["STATUS_IMPORT"]+"']") == null){
												
													var li = document.createElement("li");
														li.setAttribute("data-status-import", data["RESULT"]["RESULT"][result]["STATUS_IMPORT"]);
														li.append(data["RESULT"]["RESULT"][result]["TEXT_IMPORT"]);
														
													notesHtmlDiv.append(li);	
												
												}
												
											}

										}
										
									},
									onfailure: function(error){

										console.log(error);

									}
								});

							}
							*/
						</script>
						
					</td>
				
				</tr>

				<script>
					
					var oBXFileDialog = new BXFileDialog();
					var oConfig;
					var UserConfig;
						
					UserConfig = {
						site : 's1',
						path : '',
						view : 'list',
						sort : 'type',
						sort_order : 'asc'
					};
					
					function callBackFileDialogPriceList(filename, path, site){

						if(filename.split('.').pop() == "xml"){

							var linkToFile = path + "/" + filename;

								inputPaste.value = linkToFile;

						}else{

							alert("Можно выбирать только XML файлы");

							return false;

						}

					}

					function callBackFileDialogXsd(filename, path, site){

						if(filename.split('.').pop() == "xsd"){

							var linkToFile = path + "/" + filename;

								inputPaste.value = linkToFile;

						}else{

							alert("Можно выбирать только XSD файлы");

							return false;

						}

					}
							
					function browseFile(input, openTo){
						
						if(openTo == "xsd"){
							
							window.inputPaste = input.previousSibling.previousSibling;
							
							var submitFuncName = "callBackFileDialogXsd";
							
						}
						
						if(openTo == "priceList"){
							
							window.inputPaste = input.previousSibling.previousSibling.previousSibling.previousSibling.previousSibling.previousSibling;
							
							var submitFuncName = "callBackFileDialogPriceList";
							
						}
												
						oConfig = {
							submitFuncName : submitFuncName,
							select : 'F',
							operation: 'O',
							showUploadTab : true,
							showAddToMenuTab : false,
							site : 's1',
							path : '/upload/price/sellers',
							lang : 'ru',
							fileFilter : '',
							allowAllFiles : true,
							saveConfig : true,
							sessid: "<?=bitrix_sessid();?>",
							checkChildren: true,
							genThumb: true,
							zIndex: 2500				
						};

						oBXFileDialog.Open(oConfig, UserConfig);
						
					}
					
				</script>
				
			<!-- Вкладка "Прайслисты на удалённом сервере" -->
			<?$tabControl->BeginNextTab();?>
	
				<tr>	
						
					<td>
						
						<table class="xml-table">
							
							<?if(!empty($sellerXmlInfo)){?>
							
							<?$i = 1;?>
							
							<tr class="xml-table-tr-main">
									
								<td>
									
									<table class="xml-table-tr-main-table">
										
										<tr>

											<td>#</td>
											<td>Название прайс-листа</td>
											<td>Путь к прайс-листу</td>
											<td>XSD схема</td>
											<td>Действия</td>
											<td>Информация</td>

										</tr>
										
										<?foreach($sellerXmlInfo as $xml){?>

											<tr class="xml-table-tr-main-table-id-<?=$xml["ID"];?>">

												<td><?=$i;?></td>
												<td><?=$xml["FILE_XML_NAME_SELLER"];?></td>
												<td>
													Наш сервер: <?=$xml["FILE_XML_LINK_SELLER"];?>
													
													<?if(!empty($xml["FILE_XML_LINK_SELLER"])){?>
													
														<br/>
														Сервер поставщика: <?=$xml["FILE_XML_LINK_SELLER_LINK"];?><br/>
														
													<?}?>
												</td>
												<td><?=$sellerXsdInfo[$xml["ID_XSD"]]["FILE_XSD_NAME_SELLER"];?></td>
												<td align="center" valign="top">
													
													<input type="button" name="importSettings" value="Настройки импорта" onclick="importSettings(<?=$xml["ID"];?>)" <?if($xml["STEPS"]["AUTOUPDATE"] == "Y"){?>disabled<?}?>>
													
													<input type="button" name="timeXml" value="Настройка расписания импорта" OnClick="timeXml(<?=$xml["ID"];?>);">
													
													<input type="button" name="deleteXml" value="Удалить прайс-лист" OnClick="deleteXml(<?=$xml["ID"];?>);" <?if($xml["STEPS"]["AUTOUPDATE"] == "Y"){?>disabled<?}?>>
	
													<?if($xml["STEPS"]["ISSET_SETTINGS"] == "Y"){?>
													
														<input type="button" name="importPriceList" onclick="importPriceList(<?=$xml["ID"];?>)" value="Импортировать" <?if($xml["STEPS"]["AUTOUPDATE"] == "Y"){?>disabled<?}?>>
														
													<?}?>
													
												</td>
												
												<td class="note-controls parsing-note" valign="top">
														
													<div class="errors-tab-3-1">

														<ul></ul>

													</div>
													
												</td>

											</tr>
											
										<?$i++;?>
								
										<?}?>
							
									</table>
										
								</td>
									
							</tr>
							
							<tr>
								
								<td>
							
									<br/><br/>
									
								</td>

							</tr>
							
							<?}?>
							
							<tr>
								
								<td>
									
									Добавить прайс-лист
									
								</td>
								
							</tr>
							
							<tr class="xml-table-tr">
									
								<td>
									
									<span class='count-row'>#1</span>
								
									<input type="text" name="FILE_NEW_XML_NAME" size="30" maxlength="500" value="" placeholder="Наименование прайс-листа">
									<input type="text" name="FILE_NEW_XML_FILE" size="30" maxlength="500" value="" placeholder="Путь к файлу на сервере">
									<input type="text" name="FILE_NEW_XML_FILE_LINK" size="30" maxlength="500" value="" placeholder="URL файла у поставщика">
									
									<select name="XSD_SCHEMA">
										
										<option value="">Выбери XSD схему</option>
										
										<?foreach($sellerXsdInfo as $item){?>
										
											<option value="<?=$item["ID"];?>"><?=$item["FILE_XSD_NAME_SELLER"];?></option>
										
										<?}?>
										
									</select>
									
									<input type="button" name="browse" value="Выбрать / Загрузить прайс" OnClick="browseFile(this, 'priceList');">
									<input type="button" name="deleteRow" value="Удалить строку" OnClick="deleteRowXml(this);">
									
								</td>
									
							</tr>
							
							<tr class="xml-table-tr-new">
								
								<td>
								
									<input type="button" name="browse" value="Добавить строку" OnClick="addNewRowXml();">
								
								</td>
								
							</tr>
							
							<tr>
								
								<td>
								
									<br/>
									
									<input type="button" name="saveXmlSeller" value="Сохранить" onclick="saveXmlSeller();">
									
									<br/><br/>
									
									<div class="errors-tab-3"><ul></ul></div>
									
								</td>
								
							</tr>
								
						</table>
							
					</td>	
					
				</tr>

			<?$tabControl->BeginNextTab();?>
		  	
				<tr>	
						
					<td>
			
						фів
						
					</td>

				</tr>
					
		<?}?>
  
	<!-- Закрыли разметку вкладок -->
	<?$tabControl->End();?>
	
	<style>
	
		.errors-tab-1 ul, .errors-tab-2 ul, .errors-tab-3 ul, .errors-tab-3-1 ul, .errors-tab-4 ul, .errors-tab-5 ul{
			padding:0;
			margin:0;
			color:red;
			list-style-type: none;
		}
		
		.errors-tab-3-1 li{
			margin-bottom:10px;
		}
		
		#edit2_edit_table .adm-info-message{
			margin:0;
		}
		
		.xsd-table, xml-table{
			width:100%;
			margin: 20px 0px 0px 0px !important;
		}
		
		.xsd-table-tr-main-table, .xml-table-tr-main-table{
			border-collapse: collapse;
			background-color:#fff;
		}
		
		.xsd-table-tr-main-table td, .xml-table-tr-main-table td{
			border: 1px solid #c4ced2;
			padding: 10px;
		}
		
		.note-controls{
		}
		
		.xml-table-tr-main-table input{
			display:block !important;
		}
		
		.treeline > li:not(:only-child), .treeline li li, .treeline-folder > li:not(:only-child), .treeline-folder li li {
			padding: 5px 0px 10px 20px;
		}
		
		.treeline ul{
			margin: 15px 0px 0px 7px;
		}
		
		.link-span{
			cursor:default;
		}
		
		.bx-core-adm-dialog-content-wrap-inner .adm-designed-checkbox-label{
			padding-left:20px;
			width:auto !important;
			font-size:10px;
		}
		
		.dont-import-display-block{
			display: block;
			float: left;
			margin-right: 10px;
		}
		
		#SAVE_CATEGORY_PRICE{
			width:100%;
		}
		
		#propertiesListChuse{
			margin-top:20px;
		}
		
		#propertiesListChuse .propertiesListChuseIblock{
			margin:0;
			padding:0px 15px;
		}
		
		#propertiesListChuse .propertiesListChuseIblock li{
			margin: 10px 0px 10px 0px;
			border-bottom:1px solid #bac9cc;
			padding-bottom:10px;
		}
		
		.main-and-offer-title-iblock{
			font-style:italic !important;
			font-weight:bold !important;
		}
		
		.allPropertiesInIblockAndOffer{
			margin-top:20px;
		}
		
		.main-left{
			float:left;
			margin-right:20px;
		}
		
		.name-iblock{
			font-size:20px !important;
		}
		
		.main-option-list, .other-option-list{
			padding:0;
			margin:0;
			list-style-type:none;
		}
		
		.main-option-list li, .other-option-list li{
			margin-bottom:5px;
		}
		
		.main-option-list li label, .other-option-list li label{
			font-size:inherit !important;
		}
		
		.title-options{
			font-weight:bold !important;
			margin-top: 10px;
			margin-bottom: 5px;
		}
		
		.xml-table-tr-main-table input[type='button']{
			width:100%;
			margin-bottom:20px;
		}
		
		.xml-table-tr-main-table input[type='button']:last-child{
			margin-bottom:0px;
		}
		
		.table-get-warnings-price-list{
			width:100%;
			border-collapse: collapse;
		}
		
		.parsing-note ul{
			color:#000;
			list-style-type: decimal;
			text-align: left;
			margin:auto;
			padding:20px;			
		}
		
		.popup-settings-table td{
			vertical-align:top;
		}
		
		.searchInListDiv{
			display: inline-block;
			position:relative;
			margin-left:10px;
		}
		
		.searchInListDivPopup{
			position:absolute;
			width:100%;
			background-color:#fff;
			top:30px;
			z-index: 1;
		}
		
		.searchInListDivPopup ul{
			margin: 0;
			padding: 0px 20px;
		}
		
		.searchInListDivPopup ul li:hover{
			cursor:pointer;
		}
		
		input[name='importPriceList']{
			margin-top:40px !important;
		}
		
		#bx-admin-prefix .adm-designed-checkbox-label{
			height: auto !important;
		}
		
		.space-absolute{
			color: #000;
			background: #fed800;
			line-height: 35px;
			padding-bottom: 5px;
			z-index: 999;
			vertical-align: middle;
			font-size: 12px;
			padding-left: 15px;
			font-family: 'Open Sans', "Helvetica Neue", Helvetica, Arial, sans-serif;
		}
		
	</style>

<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';?>	