<?
	require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_before.php';
	
	use \Bitrix\Main\Localization\Loc;
	use \Bitrix\Main\Loader;
	
	Loader::includeModule("xunit.xmldropshipping");
	Loc::LoadMessages(__FILE__);
	
	$APPLICATION->SetTitle("Поставщики");	
	
	$tableName = "sellers_xunit"; // ID таблицы
	$adminSort = new CAdminSorting($tableName, "ID", "desc"); // объект сортировки
	$adminLinst = new CAdminList($tableName, $adminSort); // основной объект списка
	
	$filterArr = [
		"find_id",
		"find_name"
	];

	$adminLinst->InitFilter($filterArr);

	$rsData = \Xunit\Xmldropshipping\SellersTable::getList();
	$rsData = new CAdminResult($rsData, $tableName);
	$rsData->NavStart();
	$adminLinst->NavText($rsData->GetNavPrint(Loc::getMessage("XUNIT_XMLDROPSHIPPING_PAGES")));
	
	$adminLinst->AddHeaders(
		[
			[
				"id"		=> "ID",
				"content"  	=> "ID",
				"sort"    	=> "id",
				"default"  	=> true,
			],
			[
				"id"		=> "NAME_SELLER",
				"content"  	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_FILTER_FIND_NAME"),
				"sort"    	=> "name",
				"default"  	=> true,
			]
		]
	);
	
	while($arRes = $rsData->NavNext(true, "f_")){
  
		$row =& $adminLinst->AddRow($f_ID, $arRes); 
		
		$row->AddInputField(
			"ID",
			[
				"size" => 20
			]
		);
		
		$row->AddViewField(
			"NAME_SELLER",
			'<a href="sellers_edit.php?ID='.$f_ID.'&lang='.LANG.'">'.$f_NAME_SELLER.'</a>'
		);
 
		$arActions = [];

		$arActions[] = [
			"ICON"		=> "edit",
			"DEFAULT"	=> true,
			"TEXT"		=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_EDIT"),
			"ACTION"	=> $adminLinst->ActionRedirect("sellers_edit.php?ID=".$f_ID)
		];
  
		$arActions[] = [
			"ICON"	=> "delete",
			"TEXT"	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_DELETE"),
			"ACTION"=> "if(confirm('".Loc::getMessage("XUNIT_XMLDROPSHIPPING_DELETE_SUCCESS")."')) ".$adminLinst->ActionDoGroup($f_ID, "delete")
		];

		$arActions[] = ["SEPARATOR" => true];

		if(is_set($arActions[count($arActions)-1], "SEPARATOR")){
			unset($arActions[count($arActions)-1]);
		}
  
		$row->AddActions($arActions);

	}
	
	$adminLinst->AddFooter(
		[
			[
				"title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"),
				"value" => $rsData->SelectedRowsCount()
			],
			[
				"counter"	=> true,
				"title"		=> GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"
			],
		]
	);

	$adminLinst->AddGroupActionTable([
		"delete"	=> GetMessage("MAIN_ADMIN_LIST_DELETE"),
		"activate"	=> GetMessage("MAIN_ADMIN_LIST_ACTIVATE"),
		"deactivate"=> GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"),
	]);

	$aContext = [
		[
			"TEXT"	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_ADD_SELLER"),
			"LINK"	=> "sellers_edit.php?lang=".LANG,
			"TITLE"	=> Loc::getMessage("XUNIT_XMLDROPSHIPPING_ADD_SELLER"),
			"ICON"	=> "btn_new",
		],
	];

	$adminLinst->AddAdminContextMenu($aContext);
	
	$adminLinst->CheckListMode();
	
?>

<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php';?>

<?
	$filter = new CAdminFilter(
		$tableName."_filter",
		[
			Loc::getMessage("XUNIT_XMLDROPSHIPPING_FILTER_FIND_ID"),
			Loc::getMessage("XUNIT_XMLDROPSHIPPING_FILTER_FIND_NAME"),
		]
	);
?>


<form name="filter_form" method="get" action="<?=$APPLICATION->GetCurPage();?>">
	
	<?$filter->Begin();?>
		
		<tr>
			<td><?=Loc::getMessage("XUNIT_XMLDROPSHIPPING_FILTER_FIND_ID");?></td>
			<td>
				<input type="text" name="find_id" size="47" value="<?=htmlspecialchars($find_id);?>">
			</td>
		</tr>
		
		<tr>
			<td><?=Loc::getMessage("XUNIT_XMLDROPSHIPPING_FILTER_FIND_NAME");?></td>
			<td>
				<input type="text" name="find_name" size="47" value="<?=htmlspecialchars($find_name);?>">
			</td>
		</tr>

	<?$filter->Buttons(
		[
			"table_id" 	=> $tableName, 
			"url" 		=> $APPLICATION->GetCurPage(), 
			"form" 		=> "filter_form"
		]
	);?>
	
	<?$filter->End();?>
	
</form>


<?$adminLinst->DisplayList();?>


<?require_once $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_admin.php';?>