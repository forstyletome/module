<?

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;

$module_id = 'xunit.xmldropshipping';

Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

if($APPLICATION->GetGroupRight($module_id) < "S"){
	
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
	
}

\Bitrix\Main\Loader::includeModule($module_id);

$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

$tabs_1 = [
    [
        'DIV' 	=> 'edit1',
        'TAB' 	=> Loc::getMessage('XUNIT_XMLDROPSHIPPING_TAB_SELLERS_TITLE'),
		'TITLE'	=> Loc::getMessage('XUNIT_XMLDROPSHIPPING_TAB_SELLERS_DESCRIPTION')
    ],
    [
        "DIV" 	=> "edit2",
        "TAB" 	=> Loc::getMessage("MAIN_TAB_RIGHTS"),
        "TITLE" => Loc::getMessage("MAIN_TAB_TITLE_RIGHTS")
    ],
];

$tabControl_1 = new CAdminTabControl('tabControl', $tabs_1);




if($_SERVER["REQUEST_METHOD"] == "POST"	&& check_bitrix_sessid()){
	
	if(!empty($_POST["count_new_str"])){
		
		COption::SetOptionInt($module_id, "count_new_str", $_POST["count_new_str"]);
		
	}else{
		
		COption::SetOptionInt($module_id, "count_new_str", 1);
		
	}
	
}

$count_new_str = COption::GetOptionInt($module_id, "count_new_str");

?>

<?$tabControl_1->Begin();?>

<form method="POST" action="<?echo htmlspecialcharsbx($APPLICATION->GetCurPage().'?mid='.urlencode($module_id).'&lang='.LANGUAGE_ID)?>">

	<?$tabControl_1->BeginNextTab();?>

		<tr>
			<td>Количество новых строк при добавлении прайс-листов</td>
			<td><input type="text" name="count_new_str" value="<?=$count_new_str;?>"></td>
		</tr>
		
	<?$tabControl_1->BeginNextTab();?>

	<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");?>

	<?$tabControl_1->Buttons();?>

		<input type="submit" name="update" value="Сохранить" class="adm-btn-save">
		<input type="hidden" name="update" value="Y">
		<?=bitrix_sessid_post();?>

	<?$tabControl_1->End();?>

</form>